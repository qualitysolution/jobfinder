# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-19 12:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('JobFinderApp', '0005_unitgroup_points_limit'),
    ]

    operations = [
        migrations.AddField(
            model_name='applicant',
            name='photo',
            field=models.ImageField(blank=True, upload_to='/home/alina/Code/Python/JobFinder/media'),
        ),
    ]
