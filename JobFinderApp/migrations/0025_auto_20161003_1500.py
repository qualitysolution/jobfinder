# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-03 12:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('JobFinderApp', '0024_auto_20161003_1453'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employervacancy',
            name='created_by',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='JobFinderApp.EmployerUser'),
        ),
    ]
