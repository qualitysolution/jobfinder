# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-19 13:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('JobFinderApp', '0007_auto_20160819_1214'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applicant',
            name='photo',
            field=models.ImageField(blank=True, upload_to=''),
        ),
    ]
