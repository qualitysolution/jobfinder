# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-11 12:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('JobFinderApp', '0003_auto_20160811_1215'),
    ]

    operations = [
        migrations.CreateModel(
            name='ApplicantCVRequirements',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('priority', models.IntegerField(default=1)),
                ('applicant_cv', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='JobFinderApp.ApplicantCV')),
                ('requirement', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='JobFinderApp.Requirement')),
            ],
        ),
        migrations.CreateModel(
            name='ApplicantCVUnits',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('priority', models.IntegerField(default=1)),
                ('applicant_cv', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='JobFinderApp.ApplicantCV')),
                ('unit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='JobFinderApp.Unit')),
            ],
        ),
    ]
