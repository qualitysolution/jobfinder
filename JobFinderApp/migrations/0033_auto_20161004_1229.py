# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-04 09:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('JobFinderApp', '0032_auto_20161004_1158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employervacancy',
            name='schedule',
            field=models.IntegerField(choices=[(0, 'FullDay'), (1, 'FreeSchedule'), (2, 'Distance'), (3, 'PartTime'), (4, 'ShiftWork')]),
        ),
    ]
