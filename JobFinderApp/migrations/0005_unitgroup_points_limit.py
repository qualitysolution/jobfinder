# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-18 06:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('JobFinderApp', '0004_applicantcvrequirements_applicantcvunits'),
    ]

    operations = [
        migrations.AddField(
            model_name='unitgroup',
            name='points_limit',
            field=models.IntegerField(default=10),
        ),
    ]
