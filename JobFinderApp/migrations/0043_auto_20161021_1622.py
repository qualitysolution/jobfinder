# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-21 13:22
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('JobFinderApp', '0042_auto_20161021_1552'),
    ]

    operations = [
        migrations.RenameField(
            model_name='applicantappearances',
            old_name='hairs',
            new_name='hair',
        ),
    ]
