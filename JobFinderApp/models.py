from django.contrib.auth.models import User, UserManager
from django.db import models
from JobFinder import settings

# Enum block #

GENDER_CHOICES = (
    (0, 'Female'),
    (1, 'Male')
)

EDUCATION_CHOICES = (
    (0, 'UnfinishedSecondary'),
    (1, 'Secondary'),
    (2, 'UnfinishedSecondarySpecial'),
    (3, 'SecondarySpecial'),
    (4, 'IncompleteHigher'),
    (5, 'Higher')
)

EMPLOYEES_NUMBER_CHOICES = (
    (0, '<5'),
    (1, '5-10'),
    (2, '10-20'),
    (3, '20-50'),
    (4, '50-100'),
    (5, '>100')
)

SCHEDULE_CHOICES = (
    (0, 'FullDay'),         # Полная занятость
    (1, 'FreeSchedule'),    # Свободный график
    (2, 'Distance'),        # Удаленная работа
    (3, 'PartTime'),        # Частичная занятость
    (4, 'ShiftWork')        # Сменный график
)

EYES_CHOICES = (
    (0, 'Blue'),          # синие
    (1, 'Light Blue'),    # голубые
    (2, 'Grey'),          # серые
    (3, 'Blue Gray'),     # Сероголубые
    (4, 'Green Gray'),    # Серозеленые
    (5, 'Brown'),         # карие
    (6, 'Green')          # зеленые
)

HAIRS_CHOICES = (
    (0, 'Dark'),        #Темные
    (1, 'Blond'),       #Русые
    (2, 'Bright'),      #Светлые
    (3, 'Ginger'),      #Рыжие
    (4, 'Gray'),        #Седые
    (5, 'Hairless')     #Без волос
)

BODY_CHOICES = (
    (0, 'Thin'),        #Худощавое
    (1, 'Common'),      #Обычное
    (2, 'Sport'),       #Спортивное
    (3, 'Thick'),       #Плотное
    (4, 'Tight')        #Полное
)

WEIGHT_CHOICES = (
    (0,'<50'),
    (1,'50-55'),
    (2,'55-60'),
    (3,'60-65'),
    (4,'>65')
)

HEIGHT_CHOICES = (
    (0,'<150'),
    (1,'150-160'),
    (2,'160-170'),
    (3,'170-180'),
    (4,'>180')
)

ZODIAC_CHOICES = (
    (0,'Aries'),        #Овен
    (1,'Taurus'),       #Телец
    (2,'Twins'),        #Близнецы
    (3,'Cancer'),       #Рак
    (4,'Lion'),         #Лев
    (5,'Virgo'),        #Дева
    (6,'Libra'),        #Весы
    (7,'Scorpio'),      #Скорпиона
    (8,'Sagittarius'),  #Стрелец
    (9,'Capricorn'),    #Козерог
    (10, 'Aquarius'),   #Водолей
    (11, 'Fish')        #Рыбы
)

class Cities (models.Model):
    name = models.CharField(max_length=255)
   


class MetroLines (models.Model):
    name = models.CharField(max_length=255)
    city = models.ForeignKey(Cities, on_delete=models.CASCADE)
    color = models.CharField(max_length=7, default='#AAAAAA')

    @property
    def stations(self):
        return MetroStations.objects.filter(line_id=self.id)


class MetroStations (models.Model):
    name = models.CharField(max_length=255)
    line = models.ForeignKey(MetroLines, on_delete=models.CASCADE)


# Administrative block #

class Post(models.Model):
    name = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True)
    show_appearance = models.BooleanField(default=False)

    @property
    def requirements(self):
        return PostRequirementGroup.objects.get(post__id=self.id).group.requirements

    @property
    def unit_groups(self):
        db_groups = PostUnitGroup.objects.filter(post__id=self.id)
        if db_groups.count() < 1:
            return None
        groups = []
        for group in db_groups:
            groups.append(group.group)
        return groups




# Post requirements #

class RequirementGroup(models.Model):
    name = models.CharField(max_length=200, default="No name")
    high_rating_count = models.IntegerField(default=5)
    medium_rating_count = models.IntegerField(default=10)

    @property
    def requirements(self):
        return Requirement.objects.filter(group__id=self.id)


class Requirement(models.Model):
    group = models.ForeignKey(RequirementGroup, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)


class PostRequirementGroup(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    group = models.ForeignKey(RequirementGroup, on_delete=models.CASCADE)
    step = models.IntegerField(default=1)


# Post units #

class UnitGroup(models.Model):
    name = models.CharField(max_length=200, default="No name")
    points_limit = models.IntegerField(default=10)

    @property
    def units(self):
        return Unit.objects.filter(group__id=self.id)


class Unit(models.Model):
    group = models.ForeignKey(UnitGroup, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, default="No name")


class PostUnitGroup(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    group = models.ForeignKey(UnitGroup, on_delete=models.CASCADE)


# Applicant model block #

class ApplicationUser(User):
    patronymic = models.CharField(max_length=50, blank=True)
    objects = UserManager()


class Applicant(models.Model):
    user = models.OneToOneField(ApplicationUser, on_delete=models.CASCADE)
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.IntegerField(choices=GENDER_CHOICES)
    city = models.ForeignKey(Cities, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    subway = models.ForeignKey(MetroStations, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    nationality = models.CharField(max_length=40, blank=True)
    about = models.TextField(blank=True)
    photo = models.ImageField(default='no_avatar.png', upload_to='photo/')
    x1 = models.IntegerField(default=0)
    x2 = models.IntegerField(default=300)
    y1 = models.IntegerField(default=0)
    y2 = models.IntegerField(default=300)


    @property
    def emails(self):
        return ApplicantContactEmail.objects.filter(applicant=self)

    @property
    def phones(self):
        return ApplicantContactPhone.objects.filter(applicant=self)

    @property
    def education(self):
        return ApplicantEducation.objects.filter(applicant=self)

    @property
    def links(self):
        return ApplicantLinks.objects.filter(applicant=self)

    @property
    def cvs(self):
        return ApplicantCV.objects.filter(applicant=self)

    @property
    def appearance(self):
        return ApplicantAppearances.objects.get(applicant=self)

class ApplicantContactEmail(models.Model):
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE)
    email = models.CharField(max_length=50)
    use_for_notification = models.BooleanField(default=False)
    show_to_employer = models.BooleanField(default=True)
    confirmed = models.BooleanField(default=False)
    activation_code = models.CharField(max_length=64, null=True, blank=True)


class ApplicantContactPhone(models.Model):
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE)
    phone = models.CharField(max_length=20)
    use_for_notification = models.BooleanField(default=False)
    show_to_employer = models.BooleanField(default=True)


class ApplicantEducation(models.Model):
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE)
    education_type = models.IntegerField(choices=EDUCATION_CHOICES)
    institution_name = models.CharField(max_length=200)
    speciality = models.CharField(max_length=100)
    finish_date = models.CharField(max_length=4)

    @property
    def education_type_string(self):
        if self.education_type == 0:
            return "Неоконченное среднее"
        if self.education_type == 1:
            return "Среднее"
        if self.education_type == 2:
            return "Неоконченное среднее специальное"
        if self.education_type == 3:
            return "Среднее специальное"
        if self.education_type == 4:
            return "Неоконченное высшее"
        if self.education_type == 5:
            return "Высшее"


class ApplicantLinks(models.Model):
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE)
    link = models.TextField()
    description = models.CharField(max_length=50)

class ApplicantAppearances(models.Model):
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE)
    eyes_color = models.IntegerField(choices=EYES_CHOICES, blank=True, null=True)
    hair = models.IntegerField(choices=HAIRS_CHOICES, blank=True, null=True)
    body_type = models.IntegerField(choices=BODY_CHOICES, blank=True, null=True)
    weight = models.IntegerField(choices=WEIGHT_CHOICES, blank=True, null=True)
    height = models.IntegerField(choices=HEIGHT_CHOICES, blank=True, null=True)
    zodiac = models.IntegerField(choices=ZODIAC_CHOICES, blank=True, null=True)

class ApplicantCV(models.Model):
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    salary = models.IntegerField(default=0)
    additional_info = models.TextField(blank=True)





class ApplicantCVRequirements(models.Model):
    applicant_cv = models.ForeignKey(ApplicantCV, on_delete=models.CASCADE)
    requirement = models.ForeignKey(Requirement, on_delete=models.CASCADE)
    priority = models.IntegerField(default=1)


class ApplicantCVUnits(models.Model):
    applicant_cv = models.ForeignKey(ApplicantCV, on_delete=models.CASCADE)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    priority = models.IntegerField(default=1)



# Employer model block #

class Employer(models.Model):

    org_name = models.CharField(max_length=200)
    city = models.ForeignKey(Cities, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    subway = models.ForeignKey(MetroStations, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    address = models.CharField(max_length=200)
    site_address = models.CharField(max_length=100, blank=True)
    employees_number = models.IntegerField(choices=EMPLOYEES_NUMBER_CHOICES)
    about = models.TextField(blank=True)
    logo = models.ImageField(default='no_logo.png', upload_to='logo/')
    x1 = models.IntegerField(default=0)
    x2 = models.IntegerField(default=300)
    y1 = models.IntegerField(default=0)
    y2 = models.IntegerField(default=300)

    @property
    def emails(self):
        return EmployerContactEmail.objects.filter(employer=self)

    @property
    def phones(self):
        return EmployerContactPhone.objects.filter(employer=self)

    @property
    def users(self):
        return EmployerUser.objects.filter(employer=self)

    @property
    def vacancies(self):
        return EmployerVacancy.objects.filter(employer=self)


class EmployerContactEmail(models.Model):
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE)
    email = models.CharField(max_length=50)
    use_for_notification = models.BooleanField(default=False)
    show_to_applicant = models.BooleanField(default=True)
    confirmed = models.BooleanField(default=False)
    activation_code = models.CharField(max_length=64, null=True, blank=True)


class EmployerContactPhone(models.Model):
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE)
    phone = models.CharField(max_length=20)
    use_for_notification = models.BooleanField(default=False)
    show_to_applicant = models.BooleanField(default=True)


class EmployerUser(models.Model):
    user = models.ForeignKey(ApplicationUser, on_delete=models.CASCADE)
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE)


class EmployerVacancy(models.Model):
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE)
    created_by = models.ForeignKey(ApplicationUser, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    salary_lower = models.IntegerField(null=True, blank=True)
    salary_upper = models.IntegerField(null=True, blank=True)
    salary_by_agreement = models.BooleanField(default=False)
    schedule = models.IntegerField(choices=SCHEDULE_CHOICES)
    city = models.ForeignKey(Cities, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    subway = models.ForeignKey(MetroStations, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    description = models.TextField(blank=True)
    eyes_color = models.IntegerField(choices=EYES_CHOICES, blank=True, null=True)
    hairs = models.IntegerField(choices=HAIRS_CHOICES, blank=True, null=True)
    body_type = models.IntegerField(choices=BODY_CHOICES, blank=True, null=True)
    weight = models.IntegerField(choices=WEIGHT_CHOICES, blank=True, null=True)
    height = models.IntegerField(choices=HEIGHT_CHOICES, blank=True, null=True)
    zodiac = models.IntegerField(choices=ZODIAC_CHOICES, blank=True, null=True)

class EmployerVacancyRequirements(models.Model):
    employer_vacancy = models.ForeignKey(EmployerVacancy, on_delete=models.CASCADE)
    requirement = models.ForeignKey(Requirement, on_delete=models.CASCADE)
    priority = models.IntegerField(default=1)

class EmployerVacancyUnits(models.Model):
    employer_vacancy = models.ForeignKey(EmployerVacancy, on_delete=models.CASCADE)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    priority = models.IntegerField(default=1)

class EmailConfirmation(models.Model):
    activation_code = models.CharField(max_length=64)
    user = models.OneToOneField(ApplicationUser, on_delete=models.CASCADE)


class PasswordRestore(models.Model):
    restore_code = models.CharField(max_length=64)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

class ratioUnitRank(models.Model):
    max = models.FloatField()
    min = models.FloatField()
    counter = models.IntegerField()