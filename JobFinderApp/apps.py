from django.apps import AppConfig


class JobfinderappConfig(AppConfig):
    name = 'JobFinderApp'
