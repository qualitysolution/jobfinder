/**
 * Created by User on 09.08.2016.
 */

$(document).ready(function () {
    $('#InviteModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
        document.getElementById("spinner").style.display = "none";
    });
});


$(document).ready(function () {

    $('#invite-button').click(function () {
        document.getElementById("spinner").style.display = "block";
        $.ajax({
            type: 'POST',
            url: '/restricted_employer/invite',
            data: {
                "email": $('input#input-email-invite').val(),
            },
            success: function (data) {
                document.getElementById("spinner").style.display = "block";
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_employer/invite", function (data, status) {
                        $('div#staff').html($('div#staff', data).html());
                    });
                } else if (data.status == 1) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    swal("Готово!", data.message, "success");
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });
});
