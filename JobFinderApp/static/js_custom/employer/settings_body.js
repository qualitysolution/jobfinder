/**
 * Created by Andrei on 22.07.16.
 */
function load_phone(id) {
    $.ajax({
        type: 'GET',
        url: '/restricted_employer/get_phone',
        data: {"record_id": id},
        success: function (data) {
            var json = $.parseJSON(data);
            $('input#phone-edit-id').val(json[0]['pk']);
            $('input#input-phone-edit').val(json[0]['fields']['phone']);

            //var notify = $('input#input-use-for-notification-edit');
            var show = $('input#input-show-to-applicant-edit');
            //notify.prop('checked', json[0]['fields']['use_for_notification']);
            show.prop('checked', json[0]['fields']['show_to_applicant']);
            jQuery('#EditPhoneModal').modal('show');
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}


function remove_phone(id) {
    $.ajax({
        type: 'GET',
        url: '/restricted_employer/remove_phone',
        data: {"record_id": id},
        success: function (data) {
            $.get("/restricted_employer/settings", function (data, status) {
                $('div#phones-body').html($('div#phones-body', data).html());
            });
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}

function load_email(id) {
    $.ajax({
        type: 'GET',
        url: '/restricted_employer/get_email',
        data: {"record_id": id},
        success: function (data) {
            var json = $.parseJSON(data);
            $('input#email-edit-id').val(json[0]['pk']);
            $('input#input-email-edit').val(json[0]['fields']['email']);

            var notify = $('input#input-email-use-for-notification-edit');
            var show = $('input#input-email-show-to-applicant-edit');
            notify.prop('checked', json[0]['fields']['use_for_notification']);
            show.prop('checked', json[0]['fields']['show_to_applicant']);
            jQuery('#EditEmailModal').modal('show');
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}


function remove_email(id) {
    $.ajax({
        type: 'GET',
        url: '/restricted_employer/remove_email',
        data: {"record_id": id},
        success: function (data) {
            $.get("/restricted_employer/settings", function (data, status) {
                $('div#emails-body').html($('div#emails-body', data).html());
            });
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}
function reactivate_email(id) {
    $.ajax({
        type: 'POST',
        url: '/restricted_employer/reactivate_email',
        data: {"record_id": id},
        success: function (data) {
            $.get("/restricted_employer/settings", function (data, status) {
                $('div#emails-body').html($('div#emails-body', data).html());
            });
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
    swal("Готово!", "Сообщение отправлено вам на почту", "success");
}