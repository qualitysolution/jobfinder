$(document).ready(function($) {

    var background_animation_time = 400;
    var row_selector = 0;



    $(window).scroll(function() {
        if ($(window).width() > 1050)
            $('.slider-helper').stop().animate({
                "marginTop": ($(window).scrollTop()) + "px"
            }, background_animation_time);
    });

    // first step
    $.ajax({
            type: 'GET',
            url: '/public/get_requirements',
            data: {
                "post": $('#user_post').attr('post-id'),
                "step" : '2',
                "record_id" : $('#user_post').attr('record-id'),
                "employer" : '1'
            },
            success: function(response) {
                $('#first-step-content').show();
                $('#first-step').show();
                $('.level-slider-group').attr('high-rating-count', response.high_rating_count);
                $('.level-slider-group').attr('medium-rating-count', response.medium_rating_count);
                $('#high-selected-max').html(response.high_rating_count);
                $('#medium-selected-max').html(response.medium_rating_count);
                $('#high-selected').html(response.high_rating_count);
                $('#medium-selected').html(response.medium_rating_count);

                $('.level-slider-group').empty();
                var current_id = [];
                $.each(response.requirements, function (key, value) {
                    $('.level-slider-group').append('<div class="level-slider" value="1" id="requirement_' + value.id + '">' + value.name + '</div>');
                    current_id.push(value.id);
                });
                $.each(response.current_priority, function (key, value) {
                    for (var i=0; i < current_id.length; i+=1) {
                        if (current_id[i]==value.requirement_id) {
                            $('#requirement_' + current_id[i]).attr('value',value.priority);
                        }
                    }
                });

                $(".level-slider").each(function () {
                        var val = $(this).attr("value");
                        var id = $(this).attr("id");

                        var title = '<div class="row col-sm-12 ' + ((row_selector == 0) ? 'even-row' : 'odd-row') + '"><div style="width: 70% !important;">' + $(this).html() + '</div>';
                        row_selector = ((row_selector == 0) ? 1 : 0);
                        var slider =
                            '<div style="min-width: 90px !important; max-width: 90px !important">' +
                            '<div class="level-selector first"></div>' +
                            '<div class="level-selector second"></div>' +
                            '<div class="level-selector third"></div>' +
                            '<input type="hidden" class="level-selector-value" value="' + val + '" id="' + id + '">' +
                            '</div>' +
                            '</div>';

                        $(this).after(title + slider);
                        var temp = $(this).next().children()[1];
                        var inserted = $(temp).children()[0];
                        $(this).remove();





                        if (val == "2") {
                            $(inserted).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "transparent"}, background_animation_time);
                        }

                        if (val == "3") {
                            $(inserted).animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                    });


                $('.level-slider-group').append('<button id="to-step-2" class="btn btn-success" >Далее</button>');

                $('.level-slider-group').show();

                $("#to-step-2").click(function(){
                    step2($);
                });

                $(".first").click(function () {
                        $(this).next().next().next().val("1");
                        $(this).animate({'background-color': '#F56E6E'}, background_animation_time);
                        $(this).next().animate({'background-color': 'transparent'}, background_animation_time);
                        $(this).next().next().animate({'background-color': 'transparent'}, background_animation_time);

                        var input_group = $(this).closest(".level-slider-group");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var current_high_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });
                        $('#high-selected').text(current_high_rating);
                        $('#medium-selected').text(current_medium_rating);
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-2").prop("disabled", false);
                        else
                            $("#to-step-2").prop("disabled", true);
                    });

                $(".second").click(function () {
                        var input_group = $(this).closest(".level-slider-group");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (medium_rating > current_medium_rating) {
                            current_medium_rating++;
                            $(this).next().next().val("2");
                            $('#medium-selected').text(current_medium_rating);
                            $(this).prev().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).next().animate({"background-color": "transparent"}, background_animation_time);
                        }


                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-2").prop("disabled", false);
                        else
                            $("#to-step-2").prop("disabled", true);
                    });

                $(".third").click(function () {
                        var input_group = $(this).closest(".level-slider-group");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (high_rating > current_high_rating) {
                            current_high_rating++;
                            $(this).next().val("3");
                            $('#high-selected').text(current_high_rating);
                            $(this).prev().prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).animate({"background-color": "#5FD48C"}, background_animation_time);
                        }


                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-2").prop("disabled", false);
                        else
                            $("#to-step-2").prop("disabled", true);
                    });



            },
            error: function(xhr, status, err) {},
            complete: function(xhr, status) {

            }
        });

    // second step
    function step2($) {
        $('#progressbar-third').addClass("active");
        $('#first-step').hide();
        $('#first-step-content').hide();
        $('#first-step-table').remove();
        $('#second-step-content').show();

        $(window).scrollTop(0);

        $.ajax({
                type: 'GET',
                url: '/public/has_unit_groups',
                data: {"post": $('#user_post').attr('post-id')},
                success: function (has_groups) {
                    if (has_groups.message === "true") {
                        $.ajax({
                            type: 'GET',
                            url: '/public/get_unit_groups',
                            data: {
                                "post": $('#user_post').attr('post-id')
                            },
                            success: function (groups_data) {
                                $('#second-step-content').append('<div class="third_div"></div>');
                                var json = $.parseJSON(groups_data);
                                var third_div = $('.third_div');
                                var unitGroup = json[0]["unit_groups"];
                                $.each(unitGroup, function (index, value) {
                                    unitCounter = unitGroup[index]['units'].length;
                                    $(third_div).append(
                                        "<div class='unit_name' style='margin-bottom: 30px;'>" +
                                            "<h4>"
                                                + value['name'] +
                                            "</h4>"
                                            +   "<div class='level-slider-group-3' max_rating='" + unitCounter + "'id='unit_block_" + value['id'] + "'></div>"

                                    );



                                    var block_div = $('#unit_block_' + value['id']);
                                    var slider = '<div id="level-breaks" style="min-width: 90px !important; max-width: 170px !important">';
                                    for (var i = 1; i<=unitCounter; i++) {
                                        slider = slider + '<div class="level-selector" id="select-level-'+ i +'"></div>';
                                       }
                                    slider = slider + '<input type="hidden" class="level-selector-value" value="0"></div>';



                                    row_selector = 0;
                                    $.each(value["units"], function (idx, val) {
                                        row_selector = ((row_selector == 0) ? 1 : 0);
                                         $(block_div).append(
                                             '<div id="unit_id_'+ val['id'] +'" class="row level-item ' + ((row_selector == 0) ? 'even-row' : 'odd-row') + '"><div style="width: 70% !important;">'
                                                + val['name'] +
                                             '</div>'
                                             + slider

                                         );



                                    });


                                });

                                 // get  current priority
                                $.ajax({
                                    type: 'GET',
                                    url: '/public/current_unit_priority',
                                    data: {
                                        "post": $('#user_post').attr('post-id'),
                                        "record_id" : $('#user_post').attr('record-id'),
                                        "employer" : '1'
                                    },
                                    success : function(currentPriority) {
                                        $.each(currentPriority, function (index, value) {
                                            $('#unit_id_'+ value['unit_id']).children().children('.level-selector-value').attr('value', value['priority']);
                                            var newValue =  value['priority'];
                                            var myDiv = $('#unit_id_'+ value['unit_id']).children();
                                            var maxCount = myDiv.parent().parent().children().length;
                                            changeColors(myDiv,newValue, maxCount);
                                        });
                                    }
                                });


                                $(".level-selector").click(function () {
                                    var newValue =  $(this).attr('id').substr(13);
                                    var myDiv = $(this).parent();
                                    var maxCount = myDiv.parent().parent().children().length;


                                    changeColors(myDiv,newValue, maxCount);

                                    $.each(myDiv.parent().parent().children(), function (i, value) {
                                        var child = $(value).find("#level-breaks").find(".level-selector-value");
                                        if (child.val() == newValue) {
                                            var test = myDiv.find(".level-selector-value").attr('value');
                                            child.val(test);
                                            changeColors(child.parent(),test, maxCount);
                                        }

                                    });


                                    myDiv.find(".level-selector-value").val(newValue);
                                    var input_group = $(this).closest(".level-slider-group-3");
                                    var max_rating = $(input_group).attr("max_rating");
                                    var current_rating = 0;
                                    var children = $(input_group).find("input.level-selector-value");

                                    $.each(myDiv.parent().parent().parent().parent(), function (i, value) {
                                        check = $(value).find('.unit_name').find('.level-slider-group-3').find('.level-item').find('#level-breaks').find("input.level-selector-value");
                                        isEmpty = false;
                                        $.each(check, function(idx,value){
                                            if($(value).attr('value')== 0){
                                                isEmpty = true;
                                            }
                                        });

                                    });


                                    if (!isEmpty) {
                                        $("#to-step-3").prop("disabled", false);
                                    }
                                });


                                $("#second-step-content").append('<button  id="to-step-3" class="btn btn-success">Далее</button>')

                                $("#to-step-3").click(function () {
                                    step3($);
                                });

                            }
                        });
                    }
                    else {
                        $.ajax({
                            type: 'GET',
                            url: '/public/get_requirements',
                            data: {"post": $('#user_post').attr('post-id'),
                                    "record_id" : $('#user_post').attr('record-id'),
                                    "employer" : '1',
                                    "step":'3'},
                            success: function (response) {
                                        $('#first-step-table').remove();
                                        $('#second-step').show();
                                        $('#third-step-content').append('<div class="level-slider-group-3"  high-rating-count="10" medium-rating-count="10" style="max-width: 600px;"></div>');
                                        $('.level-slider-group-3').attr('high-rating-count', response.high_rating_count);
                                        $('.level-slider-group-3').attr('medium-rating-count', response.medium_rating_count);
                                        var current_medium_rating = 0;
                                        var current_high_rating = 0;
                                        $('#high-selected-max').html(response.high_rating_count);
                                        $('#medium-selected-max').html(response.medium_rating_count);
                                        $('#high-selected').html(response.high_rating_count);
                                        $('#medium-selected').html(response.medium_rating_count);
                                        $('.level-slider-group-3').empty();

                                        $.each(response.requirements, function (key, value) {
                                            $('.level-slider-group-3').append('<div class="level-slider" value="1" id="requirement_' + value.id + '">' + value.name + '</div>');
                                        });
                                        var current_id = [];
                                        $.each(response.requirements, function (key, value) {
                                            current_id.push(value.id);
                                        });
                                        $.each(response.current_priority, function (key, value) {
                                            for (var i = 0; i < current_id.length; i += 1) {
                                                if (current_id[i] == value.requirement_id) {
                                                    $('#requirement_' + current_id[i]).attr('value', value.priority);
                                                }
                                            }
                                        });

                                        $('.level-slider-group-3').show();

                                        $(".level-slider").each(function () {
                                            var val = $(this).attr("value");
                                            var id = $(this).attr("id");

                                            var title = '<div class="row col-sm-12 ' + ((row_selector == 0) ? 'even-row' : 'odd-row') + '"><div style="width: 70% !important;">' + $(this).html() + '</div>';
                                            row_selector = ((row_selector == 0) ? 1 : 0);
                                            var slider =
                                                '<div style="min-width: 90px !important; max-width: 90px !important">' +
                                                '<div class="level-selector first"></div>' +
                                                '<div class="level-selector second"></div>' +
                                                '<div class="level-selector third"></div>' +
                                                '<input type="hidden" class="level-selector-value" value="' + val + '" id="' + id + '">' +
                                                '</div>' +
                                                '</div>';
                                            $(this).after(title + slider);
                                            var temp = $(this).next().children()[1];
                                            var inserted = $(temp).children()[0];
                                            $(this).remove();

                                            if (val == "2") {
                                                $(inserted).animate({"background-color": "#F5C66E"}, background_animation_time);
                                                $(inserted).next().animate({"background-color": "#F5C66E"}, background_animation_time);
                                                $(inserted).next().next().animate({"background-color": "transparent"}, background_animation_time);
                                            }

                                            if (val == "3") {
                                                $(inserted).animate({"background-color": "#5FD48C"}, background_animation_time);
                                                $(inserted).next().animate({"background-color": "#5FD48C"}, background_animation_time);
                                                $(inserted).next().next().animate({"background-color": "#5FD48C"}, background_animation_time);
                                            }
                                        });



                                        $(".first").click(function () {
                                            $(this).next().next().next().val("1");
                                            $(this).animate({'background-color': '#F56E6E'}, background_animation_time);
                                            $(this).next().animate({'background-color': 'transparent'}, background_animation_time);
                                            $(this).next().next().animate({'background-color': 'transparent'}, background_animation_time);

                                            var input_group = $(this).closest(".level-slider-group-3");
                                            var high_rating = $(input_group).attr("high-rating-count");
                                            var current_high_rating = 0;
                                            var medium_rating = $(input_group).attr("medium-rating-count");
                                            var current_medium_rating = 0;
                                            var children = $(input_group).find("input.level-selector-value");
                                            $(children).each(function () {
                                                if ($(this).val() == "2")
                                                    current_medium_rating++;
                                                if ($(this).val() == "3")
                                                    current_high_rating++;
                                            });

                                            $('#high-selected').text(current_high_rating);
                                            $('#medium-selected').text(current_medium_rating);

                                            if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                                                $("#to-step-3").prop("disabled", false);
                                            else
                                                $("#to-step-3").prop("disabled", true);
                                        });

                                        $(".second").click(function () {
                                            var input_group = $(this).closest(".level-slider-group-3");
                                            var high_rating = $(input_group).attr("high-rating-count");
                                            var current_high_rating = 0;
                                            var medium_rating = $(input_group).attr("medium-rating-count");
                                            var current_medium_rating = 0;
                                            var children = $(input_group).find("input.level-selector-value");
                                            $(children).each(function () {
                                                if ($(this).val() == "2")
                                                    current_medium_rating++;
                                                if ($(this).val() == "3")
                                                    current_high_rating++;
                                            });

                                            if (medium_rating > current_medium_rating) {
                                                current_medium_rating++;
                                                $(this).next().next().val("2");
                                                $('#medium-selected').text(current_medium_rating);
                                                $(this).prev().animate({"background-color": "#F5C66E"}, background_animation_time);
                                                $(this).animate({"background-color": "#F5C66E"}, background_animation_time);
                                                $(this).next().animate({"background-color": "transparent"}, background_animation_time);
                                            }
                                            if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                                                $("#to-step-3").prop("disabled", false);
                                            else
                                                $("#to-step-3").prop("disabled", true);
                                        });

                                        $(".third").click(function () {
                                            var input_group = $(this).closest(".level-slider-group-3");
                                            var high_rating = $(input_group).attr("high-rating-count");
                                            var current_high_rating = 0;
                                            var medium_rating = $(input_group).attr("medium-rating-count");
                                            var current_medium_rating = 0;
                                            var children = $(input_group).find("input.level-selector-value");
                                            $(children).each(function () {
                                                if ($(this).val() == "2")
                                                    current_medium_rating++;
                                                if ($(this).val() == "3")
                                                    current_high_rating++;
                                            });

                                            if (high_rating > current_high_rating) {
                                                current_high_rating++;
                                                $(this).next().val("3");
                                                $('#high-selected').text(current_high_rating);
                                                $(this).prev().prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                                                $(this).prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                                                $(this).animate({"background-color": "#5FD48C"}, background_animation_time);
                                            }
                                            if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                                                $("#to-step-3").prop("disabled", false);
                                            else
                                                $("#to-step-3").prop("disabled", true);
                                        });

                                        $('.level-slider-group-3').append('<button id="to-step-3" class="btn btn-success">Далее</button>');
                                        $('#to-step-3').click(function(){
                                            step3($);
                                        });


                                }
                        });
                    }

                }
            });
    }


    // third step
    function step3($) {
        $('#progressbar-fourth').addClass("active");
        $('#second-step-content').hide();
        $('.level-slider-group-3').hide();
        $('#second-step').hide();
        $('#second-step-table').remove();
        $("#third-step").show();
        $("#third-step-content").show();

        $(window).scrollTop(0);

        $.ajax({
            type: 'GET',
            url: '/public/get_requirements',
            data: {
                "post": $('#user_post').attr('post-id'),
                "step" : '4',
                 "record_id" : $('#user_post').attr('record-id'),
                "employer" : '1'
            },
            success: function(response) {
                    $('.level-slider-group-4').attr('high-rating-count', response.high_rating_count);
                    $('.level-slider-group-4').attr('medium-rating-count', response.medium_rating_count);
                    $('#high-selected-max').html(response.high_rating_count);
                    $('#medium-selected-max').html(response.medium_rating_count);
                    $('#high-selected').html(response.high_rating_count);
                    $('#medium-selected').html(response.medium_rating_count);

                    $('.level-slider-group-4').empty();


                    $.each(response.requirements, function (key, value) {
                        $('.level-slider-group-4').append('<div class="level-slider" value="1" id="requirement_' + value.id + '">' + value.name + '</div>');

                    });

                    var current_id = [];
                    $.each(response.requirements, function (key, value) {
                        current_id.push(value.id);
                    });
                    $.each(response.current_priority, function (key, value) {
                    for (var i=0; i < current_id.length; i+=1) {
                        if (current_id[i]==value.requirement_id) {
                            $('#requirement_' + current_id[i]).attr('value',value.priority);
                        }
                    }
                });

                    $('.level-slider-group-4').show();

                    $('.level-slider-group-4').append('<button id="to-step-4" class="btn btn-success" >Далее</button>');
                    $('#to-step-4').click(function(){
                        step4($);
                    });


                    $(".level-slider").each(function () {
                        var val = $(this).attr("value");
                        var id = $(this).attr("id");

                        var title = '<div class="row col-sm-12 ' + ((row_selector == 0) ? 'even-row' : 'odd-row') + '"><div style="width: 70% !important;">' + $(this).html() + '</div>';
                        row_selector = ((row_selector == 0) ? 1 : 0);
                        var slider =
                            '<div style="min-width: 90px !important; max-width: 90px !important">' +
                            '<div class="level-selector first"></div>' +
                            '<div class="level-selector second"></div>' +
                            '<div class="level-selector third"></div>' +
                            '<input type="hidden" class="level-selector-value" value="' + val + '" id="' + id + '">' +
                            '</div>' +
                            '</div>';
                        $(this).after(title + slider);
                        var temp = $(this).next().children()[1];
                        var inserted = $(temp).children()[0];
                        $(this).remove();

                        if (val == "2") {
                            $(inserted).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "transparent"}, background_animation_time);
                        }

                        if (val == "3") {
                            $(inserted).animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                    });

                    $(".first").click(function () {
                        $(this).next().next().next().val("1");
                        $(this).animate({'background-color': '#F56E6E'}, background_animation_time);
                        $(this).next().animate({'background-color': 'transparent'}, background_animation_time);
                        $(this).next().next().animate({'background-color': 'transparent'}, background_animation_time);

                        var input_group = $(this).closest(".level-slider-group-4");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });
                        $('#high-selected').text(current_high_rating);
                        $('#medium-selected').text(current_medium_rating);
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-4").prop("disabled", false);
                        else
                            $("#to-step-4").prop("disabled", true);
                    });

                    $(".second").click(function () {
                        var input_group = $(this).closest(".level-slider-group-4");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (medium_rating > current_medium_rating) {
                            current_medium_rating++;
                            $(this).next().next().val("2");
                            $('#medium-selected').text(current_medium_rating);
                            $(this).prev().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).next().animate({"background-color": "transparent"}, background_animation_time);
                        }
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-4").prop("disabled", false);
                        else
                            $("#to-step-4").prop("disabled", true);
                    });

                    $(".third").click(function () {
                        var input_group = $(this).closest(".level-slider-group-4");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (high_rating > current_high_rating) {
                            current_high_rating++;
                            $(this).next().val("3");
                            $('#high-selected').text(current_high_rating);
                            $(this).prev().prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-4").prop("disabled", false);
                        else
                            $("#to-step-4").prop("disabled", true);
                    });
            },
            error: function(xhr, status, err) {},
            complete: function(xhr, status) {

            }
        });
    }

    // fourth step
    function step4($) {
        $(window).scrollTop(0);
        $('#progressbar-fifth').addClass("active");
        $('#third-step-content').hide();
        $('#third-step').hide();
        $('#third-step-table').remove();
        $("#fifth-step").show();
        $("#fourth-step-content").show();

        $.ajax({
            type: 'GET',
            url: '/public/get_requirements',
            data: {
                "post": $('#user_post').attr('post-id'),
                "step" : '5',
                 "record_id" : $('#user_post').attr('record-id'),
                "employer" : '1'
            },
            success: function(response) {
                    $('.level-slider-group-5').attr('high-rating-count', response.high_rating_count);
                    $('.level-slider-group-5').attr('medium-rating-count', response.medium_rating_count);
                    $('#high-selected-max').html(response.high_rating_count);
                    $('#medium-selected-max').html(response.medium_rating_count);
                    $('#high-selected').html(response.high_rating_count);
                    $('#medium-selected').html(response.medium_rating_count);

                    $('.level-slider-group-5').empty();


                    $.each(response.requirements, function (key, value) {
                        $('.level-slider-group-5').append('<div class="level-slider" value="1" id="requirement_' + value.id + '">' + value.name + '</div>');

                    });

                    var current_id = [];
                    $.each(response.requirements, function (key, value) {
                    //$('.level-slider-group-5').append('<div class="level-slider" value="1" id="requirement_' + value.id + '">' + value.name + '</div>');
                        current_id.push(value.id);
                    });
                    $.each(response.current_priority, function (key, value) {
                        for (var i=0; i < current_id.length; i+=1) {
                            if (current_id[i]==value.requirement_id) {
                                $('#requirement_' + current_id[i]).attr('value',value.priority);
                            }
                        }
                    });

                    $('.level-slider-group-5').show();

                    $('.level-slider-group-5').append('<button id="to-step-5" class="btn btn-success" >Далее</button>');
                    $('#to-step-5').click(function(){
                        step5($);
                    });


                    $(".level-slider").each(function () {
                        var val = $(this).attr("value");
                        var id = $(this).attr("id");

                        var title = '<div class="row col-sm-12 ' + ((row_selector == 0) ? 'even-row' : 'odd-row') + '"><div style="width: 70% !important;">' + $(this).html() + '</div>';
                        row_selector = ((row_selector == 0) ? 1 : 0);
                        var slider =
                            '<div style="min-width: 90px !important; max-width: 90px !important">' +
                            '<div class="level-selector first"></div>' +
                            '<div class="level-selector second"></div>' +
                            '<div class="level-selector third"></div>' +
                            '<input type="hidden" class="level-selector-value" value="' + val + '" id="' + id + '">' +
                            '</div>' +
                            '</div>';
                        $(this).after(title + slider);
                        var temp = $(this).next().children()[1];
                        var inserted = $(temp).children()[0];
                        $(this).remove();

                        if (val == "2") {
                            $(inserted).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "transparent"}, background_animation_time);
                        }

                        if (val == "3") {
                            $(inserted).animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                    });

                    $(".first").click(function () {
                        $(this).next().next().next().val("1");
                        $(this).animate({'background-color': '#F56E6E'}, background_animation_time);
                        $(this).next().animate({'background-color': 'transparent'}, background_animation_time);
                        $(this).next().next().animate({'background-color': 'transparent'}, background_animation_time);

                        var input_group = $(this).closest(".level-slider-group-5");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });
                         $('#high-selected').text(current_high_rating);
                        $('#medium-selected').text(current_medium_rating);
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-5").prop("disabled", false);
                        else
                            $("#to-step-5").prop("disabled", true);
                    });

                    $(".second").click(function () {
                        var input_group = $(this).closest(".level-slider-group-5");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (medium_rating > current_medium_rating) {
                            current_medium_rating++;
                            $(this).next().next().val("2");
                            $('#medium-selected').text(current_medium_rating);
                            $(this).prev().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).next().animate({"background-color": "transparent"}, background_animation_time);
                        }
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-5").prop("disabled", false);
                        else
                            $("#to-step-5").prop("disabled", true);
                    });

                    $(".third").click(function () {
                        var input_group = $(this).closest(".level-slider-group-5");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (high_rating > current_high_rating) {
                            current_high_rating++;
                            $(this).next().val("3");
                            $('#high-selected').text(current_high_rating);
                            $(this).prev().prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-5").prop("disabled", false);
                        else
                            $("#to-step-5").prop("disabled", true);
                    });
            },
            error: function(xhr, status, err) {},
            complete: function(xhr, status) {

            }
        });

    }

    //fifth info
    function step5($) {
        $('#progressbar-sixth').addClass("active");
        $('#fourth-step').hide();
        $('#fourth-step-content').hide();
        $('#fifth-step-table').hide();
        $('#fifth-step').hide();
        $('#fifth-step-content').show();

        $('#appearances').show();
        $(window).scrollTop(0);
        function formatState(state) {
            if (!state.id) {
                return state.text;
            }
            var $state = $(
                '<li style="display: block;"><span style="background-color:' + state.element.classList[0] + '; width: 10px; height: 10px; display: inline-block; border-radius: 5px;"></span>' + state.text + '</li>'
            );
            return $state;
        }


        $.ajax({
            type: 'GET',
            url: '/public/get_additional_info',
            data: {
                "post": $('#user_post').attr('post-id'),
                "record_id" : $('#user_post').attr('record-id'),
                "employer" : '1'
            },
            success: function (response) {
                $("#min_salary").val(response.min_salary);
                $("#max_salary").val(response.max_salary);
                if (response.by_agreement) {
                    $("#by_agreement").attr("checked", "checked");
                }
                $("#textEditorApp").Editor("setText", response.description);


            }
        });

        // $.ajax({
        //     type: 'GET',
        //     url: '/public/get_cities',
        //     success: function (data) {
        //         $.each(data, function (key, value) {
        //             $('#select-city').append("<option value='" + value.id + "'>" + value.name + "</option>");
        //         });
        //
        //     },
        //     error: function (xhr, status, err) {
        //     },
        //     complete: function (xhr, status) {
        //     }
        // });

        $('#select-city').select2({width: '100%'});
        $('#select-subway').select2({
            templateResult: formatState,
            templateSelection: formatState,
            width: '100%'
        });


        $('#select-city').change(function () {
            $('#select-subway').prop('disabled', true);
            $('#select-subway').find('option').remove();
            $('#select-subway').append('<option disabled selected value>-- Выберите станцию метро --</option>');
            $.ajax({
                type: 'GET',
                url: '/public/get_subway',
                data: {"city": $(this).val()},
                success: function (data) {
                    if (data.length > 0) {
                        $.each(data, function (key, value) {
                            $('#select-subway').append("<option value='" + value.id + "' class='" + value.line__color + "' title='" + value.line__name + "'> " + value.name + "</option>");
                        });
                        $('#select-subway').prop('disabled', false);
                    }
                },
                error: function (xhr, status, err) {
                },
                complete: function (xhr, status) {
                }
            });
        });

        $("#textEditorApp").Editor({
                'fonts': false,
                'styles': false,
                'undo': false,
                'redo': false,
                'insert_link': false,
                'unlink': false,
                'indent': false,
                'outdent': false,
                'insert_img': false,
                'block_quote': false,
                'print': false,
                'rm_format': false,
                'strikeout': false,
                'status_bar': false,
                'insert_table': false,
                'select_all': false,
                'togglescreen': false,
                'source': false,
                'splchars': false
            });

        $('#fifth-step-content').append('<button  id="to-step-7" class="btn btn-success">Готово</button>');

        $("#to-step-7").click(function () {
            saveAll(true);
        });
        // $.ajax({
        //         type : 'GET',
        //         url: '../public/get_post',
        //         data: {"post" : $('#user_post').attr('post-id')},
        //
        //         success: function (response) {
        //             if(response['show_appearance']==true){
        //                 $('#appearances').show();
        //                 $("#to-step-7").click(function () {
        //                     //form validation and sending data saveVacancy.js.
        //                     console.log('true');
        //                     saveAll(true);
        //
        //
        //                 });
        //             }
        //             else {
        //                  $("#to-step-7").click(function () {
        //                     //form validation and sending data saveVacancy.js
        //                                                  console.log('false');
        //
        //                     saveAll(false);
        //
        //                 });
        //             }
        //
        //         }
        // });

    }


});
