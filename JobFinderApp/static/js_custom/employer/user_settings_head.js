/**
 * Created by User on 05.08.2016.
 */

$(document).ready(function ($) {

    $("#show-hide").click(function () {
        if ($("#input-password").attr("type") == "password") {
            $("#input-password").attr("type", "text");
            $("#show-hide").attr("title", "Скрыть пароль");
        } else {
            $("#input-password").attr("type", "password");
            $("#show-hide").attr("title", "Показать пароль");
        }
        $("#eye-icon").toggleClass("glyphicon-eye-open");
        $("#eye-icon").toggleClass("glyphicon-eye-close");
    });
    $("#show-hide-confirm").click(function () {
        if ($("#input-password-confirm").attr("type") == "password") {
            $("#input-password-confirm").attr("type", "text");
            $("#show-hide-confirm").attr("title", "Скрыть пароль");
        } else {
            $("#input-password-confirm").attr("type", "password");
            $("#show-hide-confirm").attr("title", "Показать пароль");
        }
        $("#eye-icon-confirm").toggleClass("glyphicon-eye-open");
        $("#eye-icon-confirm").toggleClass("glyphicon-eye-close");
    });
});
jQuery(function ($) {

    $('#input-first-name').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (value.length < 2 || value.length > 30 || !pattern.test(value)) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });

    $('#input-last-name').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (value.length < 2 || value.length > 30 || !pattern.test(value)) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });

    $('#input-patronymic').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (value.length < 2 || value.length > 50 || (value.length != 0 && !pattern.test(value))) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });

    $('#input-password').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if (value.length < 6 && value.length > 0) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });

    $('#input-password-confirm').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if ((value.length < 6 && value.length > 0) || value != $('#input-password').val()) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });

    $('#submit-save').click(function (e) {
        e.preventDefault();
        var valid = true;

        var firstName = $('#input-first-name');
        var lastName = $('#input-last-name');
        var patronymic = $('#input-patronymic');
        var password = $('#input-password');
        var passwordConfirm = $('#input-password-confirm');

        if (firstName.val().length < 2 || firstName.val().length > 30) {
            firstName.parent().removeClass('has-success');
            firstName.parent().addClass('has-error');
            valid = false;
        } else {
            firstName.parent().removeClass('has-error');
            firstName.parent().addClass('has-success');
        }
        if (lastName.val().length > 30) {
            lastName.parent().removeClass('has-success');
            lastName.parent().addClass('has-error');
            valid = false;
        } else {
            lastName.parent().removeClass('has-error');
            lastName.parent().addClass('has-success');
        }
        if (patronymic.val().length > 200) {
            patronymic.parent().removeClass('has-success');
            patronymic.parent().addClass('has-error');
            valid = false;
        } else {
            patronymic.parent().removeClass('has-error');
            patronymic.parent().addClass('has-success');
        }
        if (password.val().length < 6 && password.val().length > 0) {
            password.parent().removeClass('has-success');
            password.parent().addClass('has-error');
            valid = false;
        } else {
            password.parent().removeClass('has-error');
            password.parent().addClass('has-success');
        }
        if ((passwordConfirm.val().length < 6 && passwordConfirm.val().length > 0) || passwordConfirm.val() != password.val()) {
            passwordConfirm.parent().removeClass('has-success');
            passwordConfirm.parent().addClass('has-error');
            valid = false;
        } else {
            passwordConfirm.parent().removeClass('has-error');
            passwordConfirm.parent().addClass('has-success');
        }

        if (valid) {
            $("#settings-form").submit();
            $.post("/restricted_employer/user_settings", function (data, status) {
                $('form#settings-form').html($('form#settings-form', data).html());
            });
        }
    });

})