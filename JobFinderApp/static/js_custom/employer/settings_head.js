/**
 * Created by Andrei on 13.07.16.
 */
$(document).ready(function ($) {
    Init($);
});

function Resize($) {
    var content = $("#page-content-wrapper");
    var sidebar = $("#sidebar-wrapper");
    var navbar = $(".navbar-header");

    if (content.height() + 40 > $(window).height())
        sidebar.css("height", content.height() + 40);
    else
        sidebar.css("height", $(window).height() - navbar.height());
}

function Init($) {
    $("#input-phone").mask("+7 (999) 999-9999");
    $("#input-phone-edit").mask("+7 (999) 999-9999");

    $("#textEditor").Editor({
        'fonts': false,
        'styles': false,
        'undo': false,
        'redo': false,
        'insert_link': false,
        'unlink': false,
        'indent': false,
        'outdent': false,
        'insert_img': false,
        'block_quote': false,
        'print': false,
        'rm_format': false,
        'strikeout': false,
        'status_bar': false,
        'insert_table': false,
        'select_all': false,
        'togglescreen': false,
        'source': false,
        'splchars': false
    });

    $('.Editor-editor').html($('#input-organization-about').val());

    $('[data-toggle="popover"]').popover({
        placement: 'top',
        container: 'body'
    });

    $('#AddPhoneModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
    });
    $('#EditPhoneModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
    });
    $('#AddEmailModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
        $(this).find("label#emailerror").text('').end();
    });
    $('#EditEmailModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
    });

    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<li style="display: block;"><span style="background-color:' + state.element.classList[0] + '; width: 10px; height: 10px; display: inline-block; border-radius: 5px;"></span>' + state.text + '</li>'
        );
        return $state;
    }

    $('#select-country').select2({width: '100%'});
    $('#select-region').select2({width: '100%'});
    $('#select-city').select2({width: '100%'});
    $('#select-subway').select2({
        templateResult: formatState,
        templateSelection: formatState,
        width: '100%'
    });
        // $.ajax({
        //     type: 'GET',
        //     url: '/public/get_cities',
        //     success: function (data) {
        //         $.each(data, function (key, value) {
        //             $('#select-city').append("<option value='" + value.id + "'>" + value.name + "</option>");
        //         });
        //
        //     },
        //     error: function (xhr, status, err) {
        //     },
        //     complete: function (xhr, status) {
        //     }
        // });

    $('#add-phone-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_employer/add_phone',
            data: {
                "phone": $('input#input-phone').val(),
                //"use_for_notification": $('input#input-use-for-notification').is(':checked'),
                "show_to_applicant": $('input#input-show-to-applicant').is(':checked'),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_employer/settings", function (data, status) {
                        $('div#phones-body').html($('div#phones-body', data).html());
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });
    $('#edit-phone-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_employer/edit_phone',
            data: {
                "record_id": $('input#phone-edit-id').val(),
                "phone": $('input#input-phone-edit').val(),
                //"use_for_notification": $('input#input-use-for-notification-edit').is(':checked'),
                "show_to_applicant": $('input#input-show-to-applicant-edit').is(':checked'),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_employer/settings", function (data, status) {
                        $('div#phones-body').html($('div#phones-body', data).html());
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#add-email-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_employer/add_email',
            data: {
                "email": $('input#input-email').val(),
                "use_for_notification": $('input#input-email-use-for-notification').is(':checked'),
                "show_to_applicant": $('input#input-email-show-to-applicant').is(':checked'),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_employer/settings", function (data, status) {
                        $('div#emails-body').html($('div#emails-body', data).html());
                    });
                } else if (data.status == 1) {
                    $('#idapmes').text(data.message);
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#edit-email-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_employer/edit_email',
            data: {
                "record_id": $('input#email-edit-id').val(),
                "email": $('input#input-email-edit').val(),
                "use_for_notification": $('input#input-email-use-for-notification-edit').is(':checked'),
                "show_to_applicant": $('input#input-email-show-to-applicant-edit').is(':checked'),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_employer/settings", function (data, status) {
                        $('div#emails-body').html($('div#emails-body', data).html());
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });


    // $('#add-email-button').click(function () {
    //     $.ajax({
    //         type: 'POST',
    //         url: '/restricted_employer/add_email',
    //         data: {
    //             "email": $('input#input-email').val(),
    //             "use_for_notification": $('input#input-email-use-for-notification').is(':checked'),
    //             "show_to_applicant": $('input#input-email-show-to-applicant').is(':checked'),
    //         },
    //         success: function (data) {
    //             if (data.status == 0) {
    //                 $("button#close-button").click();
    //                 $('.modal-backdrop').remove();
    //                 $.get("/restricted_employer/settings", function (data, status) {
    //                     $('div#emails-body').html($('div#emails-body', data).html());
    //                 });
    //             } else if (data.status == 1) {
    //                 $('#emailerror').text(data.message);
    //             } else if (data.status == 2) {
    //                 window.location.href = data.message;
    //             }
    //         },
    //         error: function (xhr, status, err) {
    //         },
    //         complete: function (xhr, status) {
    //         }
    //     });
    // });
    //
    // $('#edit-email-button').click(function () {
    //     $.ajax({
    //         type: 'POST',
    //         url: '/restricted_employer/edit_email',
    //         data: {
    //             "record_id": $('input#email-edit-id').val(),
    //             "email": $('input#input-email-edit').val(),
    //             "use_for_notification": $('input#input-email-use-for-notification-edit').is(':checked'),
    //             "show_to_applicant": $('input#input-email-show-to-applicant-edit').is(':checked'),
    //         },
    //         success: function (data) {
    //             if (data.status == 0) {
    //                 $("button#close-button").click();
    //                 $('.modal-backdrop').remove();
    //                 $.get("/restricted_employer/settings", function (data, status) {
    //                     $('div#emails-body').html($('div#emails-body', data).html());
    //                 });
    //             } else if (data.status == 1) {
    //             } else if (data.status == 2) {
    //                 window.location.href = data.message;
    //             }
    //         },
    //         error: function (xhr, status, err) {
    //         },
    //         complete: function (xhr, status) {
    //         }
    //     });
    // });
}

jQuery(function ($) {
// Form validation
    $('#input-organization-name').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if (value.length < 2 || value.length > 200) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-organization-site').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if (value.length > 100) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-organization-address').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if (value.length > 200) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });

    $('#submit-save').click(function (e) {
        e.preventDefault();
        var valid = true;

        var orgName = $('#input-organization-name');
        var orgSite = $('#input-organization-site');
        var orgAddress = $('#input-organization-address');

        if (orgName.val().length < 2 || orgName.val().length > 200) {
            orgName.parent().removeClass('has-success');
            orgName.parent().addClass('has-error');
            valid = false;
        } else {
            orgName.parent().removeClass('has-error');
            orgName.parent().addClass('has-success');
        }
        if (orgSite.val().length > 100) {
            orgSite.parent().removeClass('has-success');
            orgSite.parent().addClass('has-error');
            valid = false;
        } else {
            orgSite.parent().removeClass('has-error');
            orgSite.parent().addClass('has-success');
        }
        if (orgAddress.val().length > 200) {
            orgAddress.parent().removeClass('has-success');
            orgAddress.parent().addClass('has-error');
            valid = false;
        } else {
            orgAddress.parent().removeClass('has-error');
            orgAddress.parent().addClass('has-success');
        }

        if (valid) {
            $('#input-organization-about').val($('.Editor-editor').html());
            $("#settings-form").submit();
            $.get("/restricted_employer/employer_settings", function (data, status) {
                $('form#settings-form').html($('form#settings-form', data).html());
            });
        }
    });

    $('#select-country').change(function () {
        $('#select-region').prop('disabled', true);
        $('#select-region').find('option').remove();
        $('#select-region').append('<option disabled selected value>-- Выберите регион --</option>');
        $('#select-city').prop('disabled', true);
        $('#select-city').find('option').remove();
        $('#select-city').append('<option disabled selected value>-- Выберите город --</option>');
        $('#select-subway').prop('disabled', true);
        $('#select-subway').find('option').remove();
        $('#select-subway').append('<option disabled selected value>-- Выберите станцию метро --</option>');
        $.ajax({
            type: 'GET',
            url: '/public/get_regions',
            data: {"country": $(this).val()},
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#select-region').append("<option value='" + value.id + "'>" + value.name + "</option>");
                });
                $('#select-region').prop('disabled', false);
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#select-region').change(function () {
        $('#select-city').prop('disabled', true);
        $('#select-city').find('option').remove();
        $('#select-city').append('<option disabled selected value>-- Выберите город --</option>');
        $('#select-subway').prop('disabled', true);
        $('#select-subway').find('option').remove();
        $('#select-subway').append('<option disabled selected value>-- Выберите станцию метро --</option>');
        $.ajax({
            type: 'GET',
            url: '/public/get_cities',
            data: {"region": $(this).val()},
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#select-city').append("<option value='" + value.id + "'>" + value.name + "</option>");
                });
                $('#select-city').prop('disabled', false);
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#select-city').change(function () {
        $('#select-subway').prop('disabled', true);
        $('#select-subway').find('option').remove();
        $('#select-subway').append('<option disabled selected value>-- Выберите станцию метро --</option>');
        $.ajax({
            type: 'GET',
            url: '/public/get_subway',
            data: {"city": $(this).val()},
            success: function (data) {
                if (data.length > 0) {
                    $.each(data, function (key, value) {
                        $('#select-subway').append("<option value='" + value.id + "' class='" + value.line__color + "' title='" + value.line__name + "'> " + value.name + "</option>");
                    });
                    $('#select-subway').prop('disabled', false);
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#select-org').change(function () {
        $.ajax({
            type: 'GET',
            url: '/restricted_employer/employer_settings',
            data: {"organizationId": $(this).val()},
            success: function (data) {
                $('div.tab-content').html($('div.tab-content', data).html());
                $('input#input-modal-org-id').val($('#input-organization-id').val());
                Init($);
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#add-employer-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_employer/register_employer',
            data: {
                "organization_name": $('input#input-organization-name').val(),
                "organization_site": $('input#input-organization-site').val(),
                "employees_count": $('select#select-employees-count').val(),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_employer/employer_settings", function (data, status) {
                        $('div#page-content-wrapper').html($('div#page-content-wrapper', data).html());
                        Init($);
                        Resize($);
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    var image_select;

    function handleFileSelect(evt) {
        var files = evt.target.files;
        var reader = new FileReader();
        var f = files[0];
        reader.onload = (function (theFile) {
            return function (e) {
                $('#span').remove();
                var span = document.createElement('span');
                span.innerHTML = ['<img id="span" style="max-height: 400px; max-width: 568px" class="thumb" src="', e.target.result,
                    '" title="', escape(theFile.name), '"/>'].join('');
                document.getElementById('list').insertBefore(span, null);
                image_span = $('#span');

                image_select = $(image_span).imgAreaSelect({
                    aspectRatio: '1:1',
                    instance: true,
                    onSelectEnd: function (img, selection) {
                        var y_ratio = $(image_span).get(0).naturalHeight / $(image_span).get(0).height;
                        var x_ratio = $(image_span).get(0).naturalWidth / $(image_span).get(0).width;

                        $('#x1').val(Math.round(selection.x1 * x_ratio));
                        $('#y1').val(Math.round(selection.y1 * y_ratio));
                        $('#x2').val(Math.round(selection.x2 * x_ratio));
                        $('#y2').val(Math.round(selection.y2 * y_ratio));
                        if (selection.height == 0 && selection.width == 0)
                            $('#set-logo').prop("disabled", "disabled");
                        else {
                            $('#set-logo').prop("disabled", false);
                        }
                    },
                    parent: "div#LogoModal"
                });
                $('#first-step').hide();
                $('#second-step').show();
            }
        })(f);
        reader.readAsDataURL(f);
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);

    function resetModal() {
        $('#first-step').show();
        $('#second-step').hide();
        $('#set-logo').prop("disabled", "disabled");

        var span = $('output#list > span');

        $(span).imgAreaSelect({remove: true});
        $(span).remove();

        $('input[type="file"]').val(null);
    }

    $('#close-logo-dlg-button').click(function () {
        resetModal();
    });

    $('#files').click(function () {
        resetModal();
    });

    $('#logo-button').click(function () {
        resetModal();
    });

    $('#set-logo').click(function () {
        var logo_fd = new FormData(document.querySelector("#logo_fd"));
        $.ajax({
            type: 'POST',
            url: '/restricted_employer/set_logo',
            data: logo_fd,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-logo-dlg-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_employer/employer_settings?organizationId=" + $('#input-organization-id').val(), function (data, status) {
                        $('div#logo-div').html($('div#logo-div', data).html());
                        show_logo();
                        resetModal();
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

});
