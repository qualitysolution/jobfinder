/**
 * Created by Andrei on 18.07.16.
 */
$(document).ready(function () {
    $("#i_am_applicant").click(function () {
        if ($("#i_am_applicant").hasClass("active")) {
            $("#i_am_applicant").removeClass("active");
            $("#applicant_block").slideUp(300);
        }
        else {
            $("#i_am_applicant").addClass("active");
            $("#i_am_employer").removeClass("active");
            $("#employer_block").slideUp(300, function () {
                $("#applicant_block").slideDown(300, function () {
                    $('html, body').animate({
                        scrollTop: $("#applicant_block").offset().top - 100
                    }, 300);
                });
            });
        }
    });
    $("#i_am_employer").click(function () {
        if ($("#i_am_employer").hasClass("active")) {
            $("#i_am_employer").removeClass("active");
            $("#employer_block").slideUp(300);
        }
        else {
            $("#i_am_applicant").removeClass("active");
            $("#i_am_employer").addClass("active");
            $("#applicant_block").slideUp(300, function () {
                $("#employer_block").slideDown(300, function () {
                    $('html, body').animate({
                        scrollTop: $("#employer_block").offset().top - 100
                    }, 300);
                });
            });
        }
    });
});