/**
 * Created by Andrei on 13.07.16.
 */
$(document).ready(function () {
    $('[data-toggle="popover"]').popover({
        placement: 'top',
        container: 'body'
    });

    $("#TextEditor").Editor({
        'fonts': false,
        'styles': false,
        'undo': false,
        'redo': false,
        'insert_link': false,
        'unlink': false,
        'indent': false,
        'outdent': false,
        'insert_img': false,
        'block_quote': false,
        'print': false,
        'rm_format': false,
        'strikeout': false,
        'status_bar': false,
        'insert_table': false,
        'select_all': false,
        'togglescreen': false,
        'source': false,
        'splchars': false
    });

    $('.Editor-editor').html($('#input-applicant-about').val());

    $('#AddEducationModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
    });
    $('#EditEducationModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
    });
    $('#AddPhoneModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
    });
    $('#EditPhoneModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
    });
    $('#AddEmailModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
        $(this).find("label#idapmes").text('').end();
    });
    $('#EditEmailModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
    });
    $('#AvatarModal').on('hidden.bs.modal', function () {
        $(this).find("input,textarea,select").val('').end();
    });
});

jQuery(function ($) {
    $.mask.definitions['f'] = '[12]';
    $("#input-date-finish").mask("f999");
    $("#input-date-finish-edit").mask("f999");
    $("#input-phone").mask("+7 (999) 999-9999");
    $("#input-phone-edit").mask("+7 (999) 999-9999");
});

$(document).ready(function ($) {
    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<li style="display: block;"><span style="background-color:' + state.element.classList[0] + '; width: 10px; height: 10px; display: inline-block; border-radius: 5px;"></span>' + state.text + '</li>'
        );
        return $state;
    }


    $('#select-city').select2({width: '100%'});
    $('#select-subway').select2({
        templateResult: formatState,
        templateSelection: formatState,
        width: '100%'
    });

    $.ajax({
            type: 'GET',
            url: '/public/get_cities',
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#select-city').append("<option value='" + value.id + "'>" + value.name + "</option>");
                });
               
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });



    $('#add-education-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_applicant/add_education',
            data: {
                "education_type": $('select#select-education-type').val(),
                "institution_name": $('input#input-institution-name').val(),
                "speciality": $('input#input-speciality').val(),
                "finish_date": $('input#input-date-finish').val(),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_applicant/settings", function (data, status) {
                        $('div#education-body').html($('div#education-body', data).html());
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#edit-education-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_applicant/edit_education',
            data: {
                "record_id": $('input#education-edit-id').val(),
                "education_type": $('select#select-education-type-edit').val(),
                "institution_name": $('input#input-institution-name-edit').val(),
                "speciality": $('input#input-speciality-edit').val(),
                "finish_date": $('input#input-date-finish-edit').val(),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_applicant/settings", function (data, status) {
                        $('div#education-body').html($('div#education-body', data).html());
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#add-phone-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_applicant/add_phone',
            data: {
                "phone": $('input#input-phone').val(),
                //"use_for_notification": $('input#input-use-for-notification').is(':checked'),
                "show_to_employer": $('input#input-show-to-employer').is(':checked'),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_applicant/settings", function (data, status) {
                        $('div#phones-body').html($('div#phones-body', data).html());
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#edit-phone-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_applicant/edit_phone',
            data: {
                "record_id": $('input#phone-edit-id').val(),
                "phone": $('input#input-phone-edit').val(),
                //"use_for_notification": $('input#input-use-for-notification-edit').is(':checked'),
                "show_to_employer": $('input#input-show-to-employer-edit').is(':checked'),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_applicant/settings", function (data, status) {
                        $('div#phones-body').html($('div#phones-body', data).html());
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#add-email-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_applicant/add_email',
            data: {
                "email": $('input#input-email').val(),
                "use_for_notification": $('input#input-email-use-for-notification').is(':checked'),
                "show_to_employer": $('input#input-email-show-to-employer').is(':checked'),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_applicant/settings", function (data, status) {
                        $('div#emails-body').html($('div#emails-body', data).html());
                    });
                } else if (data.status == 1) {
                    $('#idapmes').text(data.message);
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

    $('#edit-email-button').click(function () {
        $.ajax({
            type: 'POST',
            url: '/restricted_applicant/edit_email',
            data: {
                "record_id": $('input#email-edit-id').val(),
                "email": $('input#input-email-edit').val(),
                "use_for_notification": $('input#input-email-use-for-notification-edit').is(':checked'),
                "show_to_employer": $('input#input-email-show-to-employer-edit').is(':checked'),
            },
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_applicant/settings", function (data, status) {
                        $('div#emails-body').html($('div#emails-body', data).html());
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                    window.location.href = data.message;
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });


    $("#show-hide").click(function () {
        if ($("#input-password").attr("type") == "password") {
            $("#input-password").attr("type", "text");
            $("#show-hide").attr("title", "Скрыть пароль");
        } else {
            $("#input-password").attr("type", "password");
            $("#show-hide").attr("title", "Показать пароль");
        }
        $("#eye-icon").toggleClass("glyphicon-eye-open");
        $("#eye-icon").toggleClass("glyphicon-eye-close");
    });
    $("#show-hide-confirm").click(function () {
        if ($("#input-password-confirm").attr("type") == "password") {
            $("#input-password-confirm").attr("type", "text");
            $("#show-hide-confirm").attr("title", "Скрыть пароль");
        } else {
            $("#input-password-confirm").attr("type", "password");
            $("#show-hide-confirm").attr("title", "Показать пароль");
        }
        $("#eye-icon-confirm").toggleClass("glyphicon-eye-open");
        $("#eye-icon-confirm").toggleClass("glyphicon-eye-close");
    });
});

jQuery(function ($) {
    $(document).ready(function () {
        $('#input-birth-date').datepicker({
            format: "dd.mm.yyyy",
            autoclose: true,
            language: "ru",
            startView: 2,
            orientation: "bottom"
        });
    });

// Form validation
    $('#input-last-name').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (value.length < 2 || value.length > 40 || !pattern.test(value)) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-first-name').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (value.length < 2 || value.length > 30 || !pattern.test(value)) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-patronymic').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (value.length == 1 || value.length > 50 || (value.length != 0 && !pattern.test(value))) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-password').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if (value.length < 6 && value.length > 0) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-password-confirm').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if ((value.length < 6 && value.length > 0) || value != $('#input-password').val()) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-birth-date').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i;
        if (!pattern.test(value)) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });

    $('#select-education-type').change(function () {
        if ($(this).val() < 2)
            $('#education-speciality').hide();
        else
            $('#education-speciality').show();

    });

    $('#select-education-type-edit').change(function () {
        if ($(this).val() < 2)
            $('#education-speciality-edit').hide();
        else
            $('#education-speciality-edit').show();

    });






    $('#submit-save').click(function (e) {
        e.preventDefault();




        var valid = true;

        var lastName = $('#input-last-name');
        var firstName = $('#input-first-name');
        var patronymic = $('#input-patronymic');
        var dateOfBirth = $('#input-birth-date');
        var password = $('#input-password');
        var passwordConfirm = $('#input-password-confirm');

        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (lastName.val().length < 2 || lastName.val().length > 40 || !pattern.test(lastName.val())) {
            lastName.parent().removeClass('has-success');
            lastName.parent().addClass('has-error');
            valid = false;
        } else {
            lastName.parent().removeClass('has-error');
            lastName.parent().addClass('has-success');
        }
        if (firstName.val().length < 2 || firstName.val().length > 30 || !pattern.test(firstName.val())) {
            firstName.parent().removeClass('has-success');
            firstName.parent().addClass('has-error');
            valid = false;
        } else {
            firstName.parent().removeClass('has-error');
            firstName.parent().addClass('has-success');
        }
        if (patronymic.val().length == 1 || patronymic.val().length > 50 || (patronymic.val().length != 0 && !pattern.test(patronymic.val()))) {
            patronymic.parent().removeClass('has-success');
            patronymic.parent().addClass('has-error');
            valid = false;
        } else {
            patronymic.parent().removeClass('has-error');
            patronymic.parent().addClass('has-success');
        }
        if (password.val().length < 6 && password.val().length > 0) {
            password.parent().removeClass('has-success');
            password.parent().addClass('has-error');
            valid = false;
        } else {
            password.parent().removeClass('has-error');
            password.parent().addClass('has-success');
        }
        if ((passwordConfirm.val().length < 6 && passwordConfirm.val().length > 0) || passwordConfirm.val() != password.val()) {
            passwordConfirm.parent().removeClass('has-success');
            passwordConfirm.parent().addClass('has-error');
            valid = false;
        } else {
            passwordConfirm.parent().removeClass('has-error');
            passwordConfirm.parent().addClass('has-success');
        }
        pattern = /^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i;
        if (!pattern.test(dateOfBirth.val())) {
            dateOfBirth.parent().removeClass('has-success');
            dateOfBirth.parent().addClass('has-error');
            valid = false;
        } else {
            dateOfBirth.parent().removeClass('has-error');
            dateOfBirth.parent().addClass('has-success');
        }


        


        if (valid) {
            $('#input-applicant-about').val($('.Editor-editor').html());
            $("#settings-form").submit();
            

            $.get("/restricted_applicant/settings", function (data, status) {
                $('form#settings-form').html($('form#settings-form', data).html());
            });
            
        }



    });


    

    /*$('#select-country').change(function () {
        $('#select-region').prop('disabled', true);
        $('#select-region').find('option').remove();
        $('#select-region').append('<option disabled selected value>-- Выберите регион --</option>');
        $('#select-city').prop('disabled', true);
        $('#select-city').find('option').remove();
        $('#select-city').append('<option disabled selected value>-- Выберите город --</option>');
        $('#select-subway').prop('disabled', true);
        $('#select-subway').find('option').remove();
        $('#select-subway').append('<option disabled selected value>-- Выберите станцию метро --</option>');
        $.ajax({
            type: 'GET',
            url: '/public/get_regions',
            data: {"country": $(this).val()},
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#select-region').append("<option value='" + value.id + "'>" + value.name + "</option>");
                });
                $('#select-region').prop('disabled', false);
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });*/

    /*$('#select-city').click(function () {

        $('#select-city').prop('disabled', true);
        $('#select-city').find('option').remove();
        $('#select-city').append('<option disabled selected value>-- Выберите город --</option>');
        $('#select-subway').prop('disabled', true);
        $('#select-subway').find('option').remove();
        $('#select-subway').append('<option disabled selected value>-- Выберите станцию метро --</option>');
        $.ajax({
            type: 'GET',
            url: '/public/get_cities',
            data: {"city": $(this).val()},
            success: function (data) {
                $.each(data, function (key, value) {
                    $('#select-city').append("<option value='" + value.id + "'>" + value.name + "</option>");
                });
                $('#select-city').prop('disabled', false);
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });
    */

    $('#select-city').change(function () {
        $('#select-subway').prop('disabled', true);
        $('#select-subway').find('option').remove();
        $('#select-subway').append('<option disabled selected value>-- Выберите станцию метро --</option>');
        $.ajax({
            type: 'GET',
            url: '/public/get_subway',
            data: {"city": $(this).val()},
            success: function (data) {
                if (data.length > 0) {
                    $.each(data, function (key, value) {
                        $('#select-subway').append("<option value='" + value.id + "' class='" + value.line__color + "' title='" + value.line__name + "'> " + value.name + "</option>");
                    });
                    $('#select-subway').prop('disabled', false);
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });



    

    var image_select;
    var image_span;

    function handleFileSelect(evt) {
        var files = evt.target.files;
        var reader = new FileReader();
        var f = files[0];
        reader.onload = (function (theFile) {
            return function (e) {
                $('#span').remove();
                var span = document.createElement('span');
                span.innerHTML = ['<img id="span" style="max-height: 400px; max-width: 568px" class="thumb" src="', e.target.result,
                    '" title="', escape(theFile.name), '"/>'].join('');
                document.getElementById('list').insertBefore(span, null);
                image_span = $('#span');

                image_select = $(image_span).imgAreaSelect({
                    aspectRatio: '1:1',
                    instance: true,
                    onSelectEnd: function (img, selection) {
                        var y_ratio = $(image_span).get(0).naturalHeight / $(image_span).get(0).height;
                        var x_ratio = $(image_span).get(0).naturalWidth / $(image_span).get(0).width;

                        $('#x1').val(Math.round(selection.x1 * x_ratio));
                        $('#y1').val(Math.round(selection.y1 * y_ratio));
                        $('#x2').val(Math.round(selection.x2 * x_ratio));
                        $('#y2').val(Math.round(selection.y2 * y_ratio));
                        if (selection.height == 0 && selection.width == 0)
                            $('#set-avatar').prop("disabled", "disabled");
                        else {
                            $('#set-avatar').prop("disabled", false);
                        }
                    },
                    parent: "div#AvatarModal"
                });
                $('#first-step').hide();
                $('#second-step').show();
            }
        })(f);
        reader.readAsDataURL(f);
    }

    document.getElementById('files').addEventListener('change', handleFileSelect, false);

    function resetModal() {
        $('#first-step').show();
        $('#second-step').hide();
        $('#set-avatar').prop("disabled", "disabled");

        var span = $('output#list > span');

        $(span).imgAreaSelect({remove: true});
        $(span).remove();

        $('input[type="file"]').val(null);
    }

    $('#close-avatar-dlg-button').click(function () {
        resetModal();
    });

    $('#files').click(function () {
        resetModal();
    });

    $('#photo-button').click(function () {
        resetModal();
    });

    $('#set-avatar').click(function () {
        var fd = new FormData(document.querySelector("#fd"));
        $.ajax({
            type: 'POST',
            url: '/restricted_applicant/set_avatar',
            data: fd,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.status == 0) {
                    $("button#close-avatar-dlg-button").click();
                    $('.modal-backdrop').remove();
                    $.get("/restricted_applicant/settings", function (data, status) {
                        $('div#photo-div').html($('div#photo-div', data).html());
                        show_avatar();
                        resetModal();
                    });
                } else if (data.status == 1) {
                } else if (data.status == 2) {
                }
            },
            error: function (xhr, status, err) {
            },
            complete: function (xhr, status) {
            }
        });
    });

});

//apperances save button

$(document).ready(function ($) {
    $('#apperances-save').click(function (e) {
        $.ajax({
            type: 'POST',
            url: '/restricted_applicant/add_appearance/',
            data: {
                "eyes_color": $("#select-eyes-color").val(),
                "hair": $("#select-hair").val(),
                "body_type": $("#select-body-type").val(),
                "weight": $("#select-weight").val(),
                "height": $("#select-height").val(),
                "zodiac": $("#select-zodiac").val()

            },
            success: function (data) {


            }

        });
    });
});


