/**
 * Created by Andrei on 22.07.16.
 */
function load_education(id) {
    $.ajax({
        type: 'GET',
        url: '/restricted_applicant/get_education',
        data: {"record_id": id},
        success: function (data) {
            var json = $.parseJSON(data);
            $('input#education-edit-id').val(json[0]['pk']);
            $('select#select-education-type-edit').val(json[0]['fields']['education_type']);
            $('input#input-institution-name-edit').val(json[0]['fields']['institution_name']);
            $('input#input-speciality-edit').val(json[0]['fields']['speciality']);
            $('input#input-date-finish-edit').val(json[0]['fields']['finish_date']);

            if ($('select#select-education-type-edit').val() > 1)
                $('#education-speciality-edit').show();
            else
                $('#education-speciality-edit').hide();
            jQuery('#EditEducationModal').modal('show');
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}


function remove_education(id) {
    $.ajax({
        type: 'GET',
        url: '/restricted_applicant/remove_education',
        data: {"record_id": id},
        success: function (data) {
            $.get("/restricted_applicant/settings", function (data, status) {
                $('div#education-body').html($('div#education-body', data).html());
            });
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}

function load_phone(id) {
    $.ajax({
        type: 'GET',
        url: '/restricted_applicant/get_phone',
        data: {"record_id": id},
        success: function (data) {
            var json = $.parseJSON(data);
            $('input#phone-edit-id').val(json[0]['pk']);
            $('input#input-phone-edit').val(json[0]['fields']['phone']);

            //var notify = $('input#input-use-for-notification-edit');
            var show = $('input#input-show-to-employer-edit');
            //notify.prop('checked', json[0]['fields']['use_for_notification']);
            show.prop('checked', json[0]['fields']['show_to_employer']);
            jQuery('#EditPhoneModal').modal('show');
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}


function remove_phone(id) {
    $.ajax({
        type: 'GET',
        url: '/restricted_applicant/remove_phone',
        data: {"record_id": id},
        success: function (data) {
            $.get("/restricted_applicant/settings", function (data, status) {
                $('div#phones-body').html($('div#phones-body', data).html());
            });
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}

function load_email(id) {
    $.ajax({
        type: 'GET',
        url: '/restricted_applicant/get_email',
        data: {"record_id": id},
        success: function (data) {
            var json = $.parseJSON(data);
            $('input#email-edit-id').val(json[0]['pk']);
            $('input#input-email-edit').val(json[0]['fields']['email']);

            var notify = $('input#input-email-use-for-notification-edit');
            var show = $('input#input-email-show-to-employer-edit');
            notify.prop('checked', json[0]['fields']['use_for_notification']);
            show.prop('checked', json[0]['fields']['show_to_employer']);
            jQuery('#EditEmailModal').modal('show');
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}


function remove_email(id) {
    $.ajax({
        type: 'GET',
        url: '/restricted_applicant/remove_email',
        data: {"record_id": id},
        success: function (data) {
            $.get("/restricted_applicant/settings", function (data, status) {
                $('div#emails-body').html($('div#emails-body', data).html());
            });
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}

function reactivate_email(id) {
    swal("Готово!", "Сообщение отправлено вам на почту", "success");
    $.ajax({
        type: 'POST',
        url: '/restricted_applicant/reactivate_email',
        data: {"record_id": id},
        success: function (data) {
            $.get("/restricted_applicant/settings", function (data, status) {
                $('div#emails-body').html($('div#emails-body', data).html());
            });
        },
        error: function (xhr, status, err) {
        },
        complete: function (xhr, status) {
        }
    });
}