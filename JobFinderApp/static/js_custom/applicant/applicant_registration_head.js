/**
 * Created by Andrei on 13.07.16.
 */
$(document).ready(function () {
    $('[data-toggle="popover"]').popover({
        placement: 'top',
        container: 'body'
    });
});

$(document).ready(function () {
    $("#show-hide").click(function () {
        if ($("#input-password").attr("type") == "password") {
            $("#input-password").attr("type", "text");
            $("#show-hide").attr("title", "Скрыть пароль");
        } else {
            $("#input-password").attr("type", "password");
            $("#show-hide").attr("title", "Показать пароль");
        }
        $("#eye-icon").toggleClass("glyphicon-eye-open");
        $("#eye-icon").toggleClass("glyphicon-eye-close");
    });
    $("#show-hide-confirm").click(function () {
        if ($("#input-password-confirm").attr("type") == "password") {
            $("#input-password-confirm").attr("type", "text");
            $("#show-hide-confirm").attr("title", "Скрыть пароль");
        } else {
            $("#input-password-confirm").attr("type", "password");
            $("#show-hide-confirm").attr("title", "Показать пароль");
        }
        $("#eye-icon-confirm").toggleClass("glyphicon-eye-open");
        $("#eye-icon-confirm").toggleClass("glyphicon-eye-close");
    });
});

jQuery(function ($) {
    $(document).ready(function () {
        var datepicker = $.fn.datepicker.noConflict();
        $.fn.bootstrapDP = datepicker;
        //$('#input-birth-date').datepicker({
        $('#input-birth-date').bootstrapDP({
            format: "dd.mm.yyyy",
            autoclose: true,
            language: "ru",
            startView: 2,
            orientation: "bottom"
        });
    });

// Form validation
    $('#input-last-name').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (value.length < 2 || value.length > 40 || !pattern.test(value)) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-first-name').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (value.length < 2 || value.length > 30 || !pattern.test(value)) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-patronymic').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (value.length == 1 || value.length > 50 || (value.length != 0 && !pattern.test(value))) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-email').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        if (!pattern.test(value) || value.length == 0) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-password').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if (value.length < 6) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-password-confirm').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if (value.length < 6 || value != $('#input-password').val()) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-birth-date').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i;
        if (!pattern.test(value)) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-birth-date').change(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        var pattern = /^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i;
        if (!pattern.test(value)) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#submit-registration').click(function (e) {
        e.preventDefault();
        var valid = true;

        var lastName = $('#input-last-name');
        var firstName = $('#input-first-name');
        var patronymic = $('#input-patronymic');
        var dateOfBirth = $('#input-birth-date');
        var email = $('#input-email');
        var password = $('#input-password');
        var passwordConfirm = $('#input-password-confirm');

        var pattern = /^[a-zA-Zа-яА-Я]+-?[a-zA-Zа-яА-Я]+$/i;
        if (lastName.val().length < 2 || lastName.val().length > 40 || !pattern.test(lastName.val())) {
            lastName.parent().removeClass('has-success');
            lastName.parent().addClass('has-error');
            valid = false;
        } else {
            lastName.parent().removeClass('has-error');
            lastName.parent().addClass('has-success');
        }
        if (firstName.val().length < 2 || firstName.val().length > 30 || !pattern.test(firstName.val())) {
            firstName.parent().removeClass('has-success');
            firstName.parent().addClass('has-error');
            valid = false;
        } else {
            firstName.parent().removeClass('has-error');
            firstName.parent().addClass('has-success');
        }
        if (patronymic.val().length == 1 || patronymic.val().length > 50 || (patronymic.val().length != 0 && !pattern.test(patronymic.val()))) {
            patronymic.parent().removeClass('has-success');
            patronymic.parent().addClass('has-error');
            valid = false;
        } else {
            patronymic.parent().removeClass('has-error');
            patronymic.parent().addClass('has-success');
        }
        pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        if (!pattern.test(email.val()) || email.val().length == 0) {
            email.parent().removeClass('has-success');
            email.parent().addClass('has-error');
            valid = false;
        } else {
            email.parent().removeClass('has-error');
            email.parent().addClass('has-success');
        }
        if (password.val().length < 6) {
            password.parent().removeClass('has-success');
            password.parent().addClass('has-error');
            valid = false;
        } else {
            password.parent().removeClass('has-error');
            password.parent().addClass('has-success');
        }
        if (passwordConfirm.val().length < 6 || passwordConfirm.val() != password.val()) {
            passwordConfirm.parent().removeClass('has-success');
            passwordConfirm.parent().addClass('has-error');
            valid = false;
        } else {
            passwordConfirm.parent().removeClass('has-error');
            passwordConfirm.parent().addClass('has-success');
        }
        pattern = /^([0-9]{2})\.([0-9]{2})\.([0-9]{4})$/i;
        if (!pattern.test(dateOfBirth.val())) {
            dateOfBirth.parent().removeClass('has-success');
            dateOfBirth.parent().addClass('has-error');
            valid = false;
        } else {
            dateOfBirth.parent().removeClass('has-error');
            dateOfBirth.parent().addClass('has-success');
        }

        if (valid) {
            $("#registration-form").submit();
        }
    });
});