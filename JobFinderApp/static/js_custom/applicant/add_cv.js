$(document).ready(function ($) {

    var background_animation_time = 400;
    var row_selector = 0;

    $("#select-post").selectize({
        sortField: {
            field: 'text',
            direction: 'asc'
        },
        dropdownParent: 'body'
    });


    $(window).scroll(function () {
        if ($(window).width() > 1050)
            $('.slider-helper').stop().animate({"marginTop": ($(window).scrollTop()) + "px"}, background_animation_time);
    });


     /* business qualities step*/
    function toStep4($) {

        $('#progressbar-fourth').addClass("active");
        $('#third-step').hide();
        $('#third-step-content').hide();
        $('#fourth-step').show();
        $('#fourth-step-content').show();
        $('#third-step-table').hide();
        $('#third-step-table').remove();

        $(window).scrollTop(0);
        $.ajax({
            type: 'GET',
            url: '/public/get_requirements',
            data: {"post": $("#select-post").val(),
                    "step" : '4'
                    },
            success: function (response) {
                    $('.level-slider-group-4').attr('high-rating-count', response.high_rating_count);
                    $('.level-slider-group-4').attr('medium-rating-count', response.medium_rating_count);
                    $('#high-selected-max').html(response.high_rating_count);
                    $('#medium-selected-max').html(response.medium_rating_count);
                    $('#high-selected').html('0');
                    $('#medium-selected').html('0');

                    $('.level-slider-group-4').empty();
                    $.each(response.requirements, function (key, value) {
                        $('.level-slider-group-4').append('<div class="level-slider" value="1" id="requirement_' + value.id + '">' + value.name + '</div>');
                    });
                    $('.level-slider-group-4').show();

                    $(".level-slider").each(function () {
                        var val = $(this).attr("value");
                        var id = $(this).attr("id");

                        var title = '<div class="row col-sm-12 ' + ((row_selector == 0) ? 'even-row' : 'odd-row') + '"><div style="width: 70% !important;">' + $(this).html() + '</div>';
                        row_selector = ((row_selector == 0) ? 1 : 0);
                        var slider =
                            '<div style="min-width: 90px !important; max-width: 90px !important">' +
                            '<div class="level-selector first"></div>' +
                            '<div class="level-selector second"></div>' +
                            '<div class="level-selector third"></div>' +
                            '<input type="hidden" class="level-selector-value" value="' + val + '" id="' + id + '">' +
                            '</div>' +
                            '</div>';
                        $(this).after(title + slider);
                        var temp = $(this).next().children()[1];
                        var inserted = $(temp).children()[0];
                        $(this).remove();

                        if (val == "2") {
                            $(inserted).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "transparent"}, background_animation_time);
                        }

                        if (val == "3") {
                            $(inserted).animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                    });



                    $(".first").click(function () {
                        $(this).next().next().next().val("1");
                        $(this).animate({'background-color': '#F56E6E'}, background_animation_time);
                        $(this).next().animate({'background-color': 'transparent'}, background_animation_time);
                        $(this).next().next().animate({'background-color': 'transparent'}, background_animation_time);

                        var input_group = $(this).closest(".level-slider-group-4");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-5").prop("disabled", false);
                        else
                            $("#to-step-5").prop("disabled", true);
                    });

                    $(".second").click(function () {
                        var input_group = $(this).closest(".level-slider-group-4");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (medium_rating > current_medium_rating) {
                            current_medium_rating++;
                            $(this).next().next().val("2");
                            $('#medium-selected').text(current_medium_rating);
                            $(this).prev().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).next().animate({"background-color": "transparent"}, background_animation_time);
                        }
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-5").prop("disabled", false);
                        else
                            $("#to-step-5").prop("disabled", true);
                    });

                    $(".third").click(function () {
                        var input_group = $(this).closest(".level-slider-group-4");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (high_rating > current_high_rating) {
                            current_high_rating++;
                            $(this).next().val("3");
                            $('#high-selected').text(current_high_rating);
                            $(this).prev().prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-5").prop("disabled", false);
                        else
                            $("#to-step-5").prop("disabled", true);
                    });


                    

                    $('.level-slider-group-4').append('<button id="to-step-5" class="btn btn-success" disabled="disabled">Далее</button>');
                    $('#to-step-5').click(function(){
                        toStep5($);
                    });

                
            },
            error: function (xhr, status, err) {
                
            },
            complete: function (xhr, status) {
                
            }
        });

    }

/* personal qualities step*/
    function toStep5($) {

        $('#progressbar-fifth').addClass("active");
        $('#fourth-step').hide();
        $('#fourth-step-content').hide();
        $('#fifth-step').show();
        $('#fifth-step-content').show();
        $('#fourth-step-table').remove();
        $(window).scrollTop(0);
        $.ajax({
            type: 'GET',
            url: '/public/get_requirements',
            data: {"post": $("#select-post").val(),
                    "step" : '5'
                    },
            success: function (response) {
                    $('.level-slider-group-5').attr('high-rating-count', response.high_rating_count);
                    $('.level-slider-group-5').attr('medium-rating-count', response.medium_rating_count);
                    $('#high-selected-max').html(response.high_rating_count);
                    $('#medium-selected-max').html(response.medium_rating_count);
                    $('#high-selected').html('0');
                    $('#medium-selected').html('0');

                    $('.level-slider-group-5').empty();
                    $.each(response.requirements, function (key, value) {
                        $('.level-slider-group-5').append('<div class="level-slider" value="1" id="requirement_' + value.id + '">' + value.name + '</div>');
                    });
                    $('.level-slider-group-5').show();

                    $(".level-slider").each(function () {
                        var val = $(this).attr("value");
                        var id = $(this).attr("id");

                        var title = '<div class="row col-sm-12 ' + ((row_selector == 0) ? 'even-row' : 'odd-row') + '"><div style="width: 70% !important;">' + $(this).html() + '</div>';
                        row_selector = ((row_selector == 0) ? 1 : 0);
                        var slider =
                            '<div style="min-width: 90px !important; max-width: 90px !important">' +
                            '<div class="level-selector first"></div>' +
                            '<div class="level-selector second"></div>' +
                            '<div class="level-selector third"></div>' +
                            '<input type="hidden" class="level-selector-value" value="' + val + '" id="' + id + '">' +
                            '</div>' +
                            '</div>';
                        $(this).after(title + slider);
                        var temp = $(this).next().children()[1];
                        var inserted = $(temp).children()[0];
                        $(this).remove();

                        if (val == "2") {
                            $(inserted).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "transparent"}, background_animation_time);
                        }

                        if (val == "3") {
                            $(inserted).animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                    });



                    $(".first").click(function () {
                        $(this).next().next().next().val("1");
                        $(this).animate({'background-color': '#F56E6E'}, background_animation_time);
                        $(this).next().animate({'background-color': 'transparent'}, background_animation_time);
                        $(this).next().next().animate({'background-color': 'transparent'}, background_animation_time);

                        var input_group = $(this).closest(".level-slider-group-5");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-5").prop("disabled", false);
                        else
                            $("#to-step-5").prop("disabled", true);
                    });

                    $(".second").click(function () {
                        var input_group = $(this).closest(".level-slider-group-5");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (medium_rating > current_medium_rating) {
                            current_medium_rating++;
                            $(this).next().next().val("2");
                            $('#medium-selected').text(current_medium_rating);
                            $(this).prev().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).next().animate({"background-color": "transparent"}, background_animation_time);
                        }
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-6").prop("disabled", false);
                        else
                            $("#to-step-6").prop("disabled", true);
                    });

                    $(".third").click(function () {
                        var input_group = $(this).closest(".level-slider-group-5");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (high_rating > current_high_rating) {
                            current_high_rating++;
                            $(this).next().val("3");
                            $('#high-selected').text(current_high_rating);
                            $(this).prev().prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-6").prop("disabled", false);
                        else
                            $("#to-step-6").prop("disabled", true);
                    });


                    

                    $('.level-slider-group-5').append('<button id="to-step-6" class="btn btn-success" disabled="disabled">Далее</button>');
                    $('#to-step-6').click(function(){
                        toStep6($);
                    });

                
            },
            error: function (xhr, status, err) {
                
            },
            complete: function (xhr, status) {
                
            }
        });

    }


/* additional information step*/
    function toStep6($) {

        
        $('#progressbar-sixth').addClass("active");
        $('#fifth-step').hide();
        $('#fifth-step-content').hide();
        $('#sixth-step').show();
        $('#sixth-step-content').show();




        var min_salary = 5700;
        var max_salary = 3500000;
        var salary = min_salary;



        $( "#amount" ).val(salary);


        var mySlider = $("#salary-slider").bootstrapSlider({
            ticks: [5700, 50000, 100000, 1000000, 3500000],
            ticks_positions: [0, 30, 60, 80, 100],
            ticks_labels: ['5 700 руб.', '50 000 руб.','100 000 руб.', '1 000 000 руб.', '3 500 000 руб.'],
            ticks_snap_bounds: 30
            }

        );

        $('#amount').on("keyup", function () {
            salary = parseInt($(this).val(),10);
            if(salary > max_salary)
                salary = max_salary;
            if(salary < min_salary)
                salary = min_salary;
            mySlider.bootstrapSlider("setValue", salary);
            mySlider.refresh();
            });
         $("#salary-slider").on('slide',function (slider) {
            $( "#amount" ).val(slider.value);
            salary = slider.value;
         });
         $("#textEditorApp").Editor({
            'fonts': false,
            'styles': false,
            'undo': false,
            'redo': false,
            'insert_link': false,
            'unlink': false,
            'indent': false,
            'outdent': false,
            'insert_img': false,
            'block_quote': false,
            'print': false,
            'rm_format': false,
            'strikeout': false,
            'status_bar': false,
            'insert_table': false,
            'select_all': false,
            'togglescreen': false,
            'source': false,
            'splchars': false
        });

        //$('#sixth-step-content').append();

        $('#sixth-step-content').append('<button id="to-step-7" class="btn btn-success">Готово</button>');

        $("#to-step-7").click(function () {
            //sending all data in saveResult.js
            saveAll(false, salary);
        });



    }



    /*professional skills step*/
    function toStepThree($) {


            $('#progressbar-third').addClass("active");
            $('#second-step').hide();
            $('#second-step-content').hide();
            $('#second-step-table').hide();
            $('#second-step-table').remove();
            /*hidding select-post*/
            $("#third-step").show();
            $("#third-step-content").show();

            $("#first-step-content").hide();

            $.ajax({
                type: 'GET',
                url: '/public/has_unit_groups',
                data: {"post": $("#select-post").val()},
                success: function (has_groups) {
                    if (has_groups.message === "true") {
                        $.ajax({
                            type: 'GET',
                            url: '/public/get_unit_groups',
                            data: {"post": $("#select-post").val()},
                            success: function (groups_data) {
                                $('#third-step-content').append('<div class="third_div"></div>');

                                var json = $.parseJSON(groups_data);
                                var third_div = $('.third_div');
                                var unitGroup = json[0]["unit_groups"];

                                $.each(unitGroup, function (index, value) {
                                    unitCounter = unitGroup[index]['units'].length;
                                    $(third_div).append(
                                        "<div class='unit_name' style='margin-bottom: 30px;'>" +
                                            "<h4>"
                                                + value['name'] +
                                            "</h4>"
                                            +   "<div class='level-slider-group-3' max_rating='" + unitCounter + "'id='unit_block_" + value['id'] + "'></div>"

                                    );



                                    var block_div = $('#unit_block_' + value['id']);
                                    var slider = '<div id="level-breaks" style="min-width: 90px !important; max-width: 170px !important">';
                                    for (var i = 1; i<=unitCounter; i++) {
                                        slider = slider + '<div class="level-selector" id="select-level-'+ i +'"></div>';
                                       }
                                    slider = slider + '<input type="hidden" class="level-selector-value"  value="0"></div>';


                                    row_selector = 0;
                                    $.each(value["units"], function (idx, val) {
                                        row_selector = ((row_selector == 0) ? 1 : 0);
                                         $(block_div).append(
                                             '<div class="row level-item ' + ((row_selector == 0) ? 'even-row' : 'odd-row') + '" id="' + val['id'] + '"><div style="width: 70% !important;">'
                                                + val['name'] +
                                             '</div>'
                                             + slider

                                         );
                                    });



                                });
                                $(".level-selector").click(function () {
                                    var newValue =  $(this).attr('id').substr(13);
                                    var myDiv = $(this).parent();
                                    var maxCount = myDiv.parent().parent().children().length;


                                    changeColors(myDiv,newValue, maxCount);

                                    $.each(myDiv.parent().parent().children(), function (i, value) {
                                        var child = $(value).find("#level-breaks").find(".level-selector-value");
                                        if (child.val() == newValue) {
                                            var test = myDiv.find(".level-selector-value").attr('value');
                                            child.val(test);
                                            changeColors(child.parent(),test, maxCount);
                                        }

                                    });


                                    myDiv.find(".level-selector-value").val(newValue);
                                    var input_group = $(this).closest(".level-slider-group-3");
                                    var max_rating = $(input_group).attr("max_rating");
                                    var current_rating = 0;
                                    var children = $(input_group).find("input.level-selector-value");

                                    $.each(myDiv.parent().parent().parent().parent(), function (i, value) {
                                        check = $(value).find('.unit_name').find('.level-slider-group-3').find('.level-item').find('#level-breaks').find("input.level-selector-value");
                                        isEmpty = false;
                                        $.each(check, function(idx,value){
                                            if($(value).attr('value')== 0){
                                                isEmpty = true;
                                            }
                                        });

                                    });
                                    if (!isEmpty) {
                                        $("#to-step-4").prop("disabled", false);
                                    }
                                });

                                $("#third-step-content").append('<button disabled="disabled"  id="to-step-4" class="btn btn-success">Далее</button>')

                                $("#to-step-4").click(function () {
                                    toStep4($);
                                });

                            }
                        });
                    }
                    // skip step
                    else {
                        $.ajax({
                            type: 'GET',
                            url: '/public/get_requirements',
                            data: {"post": $("#select-post").val(),
                                    "step":'3'},
                            success: function (response) {
                                        $('#third-step-content').append('<div class="level-slider-group-3"  high-rating-count="10" medium-rating-count="10" style="max-width: 600px;"></div>');
                                        $('#third-step-table').show();
                                        $('.level-slider-group-3').attr('high-rating-count', response.high_rating_count);
                                        $('.level-slider-group-3').attr('medium-rating-count', response.medium_rating_count);
                                        $('#high-selected-max').html(response.high_rating_count);
                                        $('#medium-selected-max').html(response.medium_rating_count);
                                        $('#high-selected').html('0');
                                        $('#medium-selected').html('0');
                                        $('.level-slider-group-3').empty();
                                        $.each(response.requirements, function (key, value) {
                                            $('.level-slider-group-3').append('<div class="level-slider" value="1" id="requirement_' + value.id + '">' + value.name + '</div>');
                                        });
                                        $('.level-slider-group-3').show();

                                        $(".level-slider").each(function () {
                                            var val = $(this).attr("value");
                                            var id = $(this).attr("id");

                                            var title = '<div class="row col-sm-12 ' + ((row_selector == 0) ? 'even-row' : 'odd-row') + '"><div style="width: 70% !important;">' + $(this).html() + '</div>';
                                            row_selector = ((row_selector == 0) ? 1 : 0);
                                            var slider =
                                                '<div style="min-width: 90px !important; max-width: 90px !important">' +
                                                '<div class="level-selector first"></div>' +
                                                '<div class="level-selector second"></div>' +
                                                '<div class="level-selector third"></div>' +
                                                '<input type="hidden" class="level-selector-value" value="' + val + '" id="' + id + '">' +
                                                '</div>' +
                                                '</div>';
                                            $(this).after(title + slider);
                                            var temp = $(this).next().children()[1];
                                            var inserted = $(temp).children()[0];
                                            $(this).remove();

                                            if (val == "2") {
                                                $(inserted).animate({"background-color": "#F5C66E"}, background_animation_time);
                                                $(inserted).next().animate({"background-color": "#F5C66E"}, background_animation_time);
                                                $(inserted).next().next().animate({"background-color": "transparent"}, background_animation_time);
                                            }

                                            if (val == "3") {
                                                $(inserted).animate({"background-color": "#5FD48C"}, background_animation_time);
                                                $(inserted).next().animate({"background-color": "#5FD48C"}, background_animation_time);
                                                $(inserted).next().next().animate({"background-color": "#5FD48C"}, background_animation_time);
                                            }
                                        });



                                        $(".first").click(function () {
                                            $(this).next().next().next().val("1");
                                            $(this).animate({'background-color': '#F56E6E'}, background_animation_time);
                                            $(this).next().animate({'background-color': 'transparent'}, background_animation_time);
                                            $(this).next().next().animate({'background-color': 'transparent'}, background_animation_time);

                                            var input_group = $(this).closest(".level-slider-group-3");
                                            var high_rating = $(input_group).attr("high-rating-count");
                                            var current_high_rating = 0;
                                            var medium_rating = $(input_group).attr("medium-rating-count");
                                            var current_medium_rating = 0;
                                            var children = $(input_group).find("input.level-selector-value");
                                            $(children).each(function () {
                                                if ($(this).val() == "2")
                                                    current_medium_rating++;
                                                if ($(this).val() == "3")
                                                    current_high_rating++;
                                            });

                                            if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                                                $("#to-step-4").prop("disabled", false);
                                            else
                                                $("#to-step-4").prop("disabled", true);
                                        });

                                        $(".second").click(function () {
                                            var input_group = $(this).closest(".level-slider-group-3");
                                            var high_rating = $(input_group).attr("high-rating-count");
                                            var current_high_rating = 0;
                                            var medium_rating = $(input_group).attr("medium-rating-count");
                                            var current_medium_rating = 0;
                                            var children = $(input_group).find("input.level-selector-value");
                                            $(children).each(function () {
                                                if ($(this).val() == "2")
                                                    current_medium_rating++;
                                                if ($(this).val() == "3")
                                                    current_high_rating++;
                                            });

                                            if (medium_rating > current_medium_rating) {
                                                current_medium_rating++;
                                                $(this).next().next().val("2");
                                                $('#medium-selected').text(current_medium_rating);
                                                $(this).prev().animate({"background-color": "#F5C66E"}, background_animation_time);
                                                $(this).animate({"background-color": "#F5C66E"}, background_animation_time);
                                                $(this).next().animate({"background-color": "transparent"}, background_animation_time);
                                            }
                                            if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                                                $("#to-step-4").prop("disabled", false);
                                            else
                                                $("#to-step-4").prop("disabled", true);
                                        });

                                        $(".third").click(function () {
                                            var input_group = $(this).closest(".level-slider-group-3");
                                            var high_rating = $(input_group).attr("high-rating-count");
                                            var current_high_rating = 0;
                                            var medium_rating = $(input_group).attr("medium-rating-count");
                                            var current_medium_rating = 0;
                                            var children = $(input_group).find("input.level-selector-value");
                                            $(children).each(function () {
                                                if ($(this).val() == "2")
                                                    current_medium_rating++;
                                                if ($(this).val() == "3")
                                                    current_high_rating++;
                                            });

                                            if (high_rating > current_high_rating) {
                                                current_high_rating++;
                                                $(this).next().val("3");
                                                $('#high-selected').text(current_high_rating);
                                                $(this).prev().prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                                                $(this).prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                                                $(this).animate({"background-color": "#5FD48C"}, background_animation_time);
                                            }
                                            if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                                                $("#to-step-5").prop("disabled", false);
                                            else
                                                $("#to-step-5").prop("disabled", true);
                                        });




                                        $('.level-slider-group-3').append('<button id="to-step-4" class="btn btn-success" disabled="disabled">Далее</button>');
                                        $('#to-step-4').click(function(){
                                            toStep4($);
                                        });


                                },
                        });
                    }
                }
            });
        }

    $("#select-post").change(function () {

        $.ajax({
            type: 'GET',
            url: '/public/get_requirements',
            data: {"post": $(this).val(),
                    "step" : '2'
                    },
            success: function (response) {

                    $('.level-slider-group').attr('high-rating-count', response.high_rating_count);
                    $('.level-slider-group').attr('medium-rating-count', response.medium_rating_count);
                    $('#high-selected-max').html(response.high_rating_count);
                    $('#medium-selected-max').html(response.medium_rating_count);
                    $('#high-selected').html('0');
                    $('#medium-selected').html('0');

                    $('.level-slider-group').empty();
                    $.each(response.requirements, function (key, value) {
                        $('.level-slider-group').append('<div class="level-slider" value="1" id="requirement_' + value.id + '">' + value.name + '</div>');
                    });

                    $(".level-slider").each(function () {
                        var val = $(this).attr("value");
                        var id = $(this).attr("id");

                        var title = '<div class="row col-sm-12 ' + ((row_selector == 0) ? 'even-row' : 'odd-row') + '"><div style="width: 70% !important;">' + $(this).html() + '</div>';
                        row_selector = ((row_selector == 0) ? 1 : 0);
                        var slider =
                            '<div style="min-width: 90px !important; max-width: 90px !important">' +
                            '<div class="level-selector first"></div>' +
                            '<div class="level-selector second"></div>' +
                            '<div class="level-selector third"></div>' +
                            '<input type="hidden" class="level-selector-value" value="' + val + '" id="' + id + '">' +
                            '</div>' +
                            '</div>';
                        $(this).after(title + slider);
                        var temp = $(this).next().children()[1];
                        var inserted = $(temp).children()[0];
                        $(this).remove();

                        if (val == "2") {
                            $(inserted).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "transparent"}, background_animation_time);
                        }

                        if (val == "3") {
                            $(inserted).animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(inserted).next().next().animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                    });

                    $('.level-slider-group').append('<button id="to-step-3" class="btn btn-success" disabled="disabled">Далее</button>');
                    /*8----------------disabled="true" */

                    $('.level-slider-group').show();

                    $('#first-step').hide();
                    $('#third-step').hide();
                    $('#third-step-content').hide();
                    $('#third-step-content').html('');

                    $('#second-step').show();
                    $('#second-step-content').show();
                    $('#progressbar-second').addClass("active");
                    $('#progressbar-third').removeClass("active");
                    $('#progressbar-fourth').removeClass("active");

                    $(".first").click(function () {
                        $(this).next().next().next().val("1");
                        $(this).animate({'background-color': '#F56E6E'}, background_animation_time);
                        $(this).next().animate({'background-color': 'transparent'}, background_animation_time);
                        $(this).next().next().animate({'background-color': 'transparent'}, background_animation_time);

                        var input_group = $(this).closest(".level-slider-group");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-3").prop("disabled", false);
                        else
                            $("#to-step-3").prop("disabled", true);
                    });

                    $(".second").click(function () {
                        var input_group = $(this).closest(".level-slider-group");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (medium_rating > current_medium_rating) {
                            current_medium_rating++;
                            $(this).next().next().val("2");
                            $('#medium-selected').text(current_medium_rating);
                            $(this).prev().animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).animate({"background-color": "#F5C66E"}, background_animation_time);
                            $(this).next().animate({"background-color": "transparent"}, background_animation_time);
                        }
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-3").prop("disabled", false);
                        else
                            $("#to-step-3").prop("disabled", true);
                    });

                    $(".third").click(function () {
                        var input_group = $(this).closest(".level-slider-group");
                        var high_rating = $(input_group).attr("high-rating-count");
                        var current_high_rating = 0;
                        var medium_rating = $(input_group).attr("medium-rating-count");
                        var current_medium_rating = 0;
                        var children = $(input_group).find("input.level-selector-value");
                        $(children).each(function () {
                            if ($(this).val() == "2")
                                current_medium_rating++;
                            if ($(this).val() == "3")
                                current_high_rating++;
                        });

                        if (high_rating > current_high_rating) {
                            current_high_rating++;
                            $(this).next().val("3");
                            $('#high-selected').text(current_high_rating);
                            $(this).prev().prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).prev().animate({"background-color": "#5FD48C"}, background_animation_time);
                            $(this).animate({"background-color": "#5FD48C"}, background_animation_time);
                        }
                        if (current_high_rating == high_rating && current_medium_rating == medium_rating)
                            $("#to-step-3").prop("disabled", false);
                        else
                            $("#to-step-3").prop("disabled", true);
                    });

                    $("#to-step-3").click(function () {
                        toStepThree($);
                    });
                
            },
            error: function (xhr, status, err) {
                
            },
            complete: function (xhr, status) {
                
            }
        });
    });
});



