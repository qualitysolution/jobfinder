/**
 * Created by Andrei on 13.07.16.
 */
$(document).ready(function () {
    $('[data-toggle="popover"]').popover({
        placement: 'top',
        container: 'body'
    });
});

$(document).ready(function () {
    $("#show-hide").click(function () {
        if ($("#input-password").attr("type") == "password") {
            $("#input-password").attr("type", "text");
            $("#show-hide").attr("title", "Скрыть пароль");
        } else {
            $("#input-password").attr("type", "password");
            $("#show-hide").attr("title", "Показать пароль");
        }
        $("#eye-icon").toggleClass("glyphicon-eye-open");
        $("#eye-icon").toggleClass("glyphicon-eye-close");
    });
    $("#show-hide-confirm").click(function () {
        if ($("#input-password-confirm").attr("type") == "password") {
            $("#input-password-confirm").attr("type", "text");
            $("#show-hide-confirm").attr("title", "Скрыть пароль");
        } else {
            $("#input-password-confirm").attr("type", "password");
            $("#show-hide-confirm").attr("title", "Показать пароль");
        }
        $("#eye-icon-confirm").toggleClass("glyphicon-eye-open");
        $("#eye-icon-confirm").toggleClass("glyphicon-eye-close");
    });
});

jQuery(function ($) {
    $('#input-password').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if (value.length < 6) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#input-password-confirm').keyup(function () {
        var value = $(this).val();
        var parent = $(this).parent();
        if (value.length < 6 || value != $('#input-password').val()) {
            parent.removeClass('has-success');
            parent.addClass('has-error');
        } else {
            parent.removeClass('has-error');
            parent.addClass('has-success');
        }
    });
    $('#submit-password-restore').click(function (e) {
        e.preventDefault();
        var valid = true;

        var password = $('#input-password');
        var passwordConfirm = $('#input-password-confirm');

        if (password.val().length < 6) {
            password.parent().removeClass('has-success');
            password.parent().addClass('has-error');
            valid = false;
        } else {
            password.parent().removeClass('has-error');
            password.parent().addClass('has-success');
        }
        if (passwordConfirm.val().length < 6 || passwordConfirm.val() != password.val()) {
            passwordConfirm.parent().removeClass('has-success');
            passwordConfirm.parent().addClass('has-error');
            valid = false;
        } else {
            passwordConfirm.parent().removeClass('has-error');
            passwordConfirm.parent().addClass('has-success');
        }
        if (valid) {
            $("#registration-form").submit();
        }
    });
});