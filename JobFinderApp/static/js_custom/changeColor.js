var background_animation_time = 400;
    function getValueColor(idx, max) {
        var index = max - idx;
        var cnt = max;
        var middle = Math.floor(cnt / 2);
        var step_red = (245 - 110) / middle;
        var step_green = (110 - 245) / middle;

            if (cnt % 2 == 0) {
                var new_red = parseInt(index <= max ? 110 + (step_red * index) : 245).toString(16);
                var new_green = parseInt(index >= middle ? 245 + (step_green * (index - middle + 1)) : 245).toString(16);

            } else {
                var new_red = parseInt(index <= middle ? 110 + (step_red * index) : 245).toString(16);
                var new_green = parseInt(index > middle ? 245 + (step_green * (index - middle)) : 245).toString(16);
            }
            var new_blue = parseInt(110).toString(16);
            return "#" + new_red + new_green + new_blue;
    }

    function changeColors(levelsDiv, newLevel, maxLevels) {
        $.each( levelsDiv.children(".level-selector"), function (i, value) {
            currentLevel = $(this).attr('id').substr(13);
            if (currentLevel<=newLevel) {
                $(value).animate({'background-color': getValueColor(newLevel, maxLevels)}, background_animation_time);
            }
            else {
                $(value).animate({'background-color': 'transparent'}, background_animation_time);
            }
        });
    }
