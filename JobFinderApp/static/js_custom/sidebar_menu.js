$(document).ready(function () {
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        if (width >= 768) {
            $("#wrapper").toggleClass("toggled-2");
            if ($("#wrapper").hasClass("toggled-2")) {
                $('#menu ul').hide();
            } else {
                $('#menu ul').children('.current').parent().show();
            }
        } else {
            $("#wrapper").toggleClass("toggled");
            if ($("#wrapper").hasClass("toggled")) {
                $('#menu ul').children('.current').parent().show();
            } else {
                $('#menu ul').hide();
            }
        }
    });
    $("#menu-toggle-2").click(function (e) {


    });

    function initMenu() {
        $('#menu ul').hide();
        $('#menu ul').children('.current').parent().show();
        //$('#menu ul:first').show();
        $('#menu li a').click(
            function () {
                var checkElement = $(this).next();
                if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                    checkElement.slideUp('normal');
                    return false;
                }
                if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                    $('#menu ul:visible').slideUp('normal');
                    checkElement.slideDown('normal');
                    return false;
                }
            }
        );
    }

    $(document).ready(function () {
        initMenu();
    });
});