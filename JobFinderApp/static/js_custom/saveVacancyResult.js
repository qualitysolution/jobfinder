
    function saveAll(withAppearance) {
        if (withAppearance) {
            //form validation
            var valid = true;

            if ($('#select-city').val() == '') {
                valid = false;
                $('#city-error').show();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            else {
                $('#city-error').hide();
                valid = true;
            }

            if ($('#select-org').val() == null) {
                valid = false;
                $('#org-error').show();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            else {
                $('#org-error').hide();
                valid = true;
            }

            if ($('#select-schedule').val() == '') {
                valid = false;
                $('#schedule-error').show();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            else {
                $('#schedule-error').hide();
                valid = true;
            }

            if ($("#textEditorApp").Editor("getText") == '') {
                valid = false;
                $('#textarea-error').show();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            else {
                $('#textarea-error').hide();
                valid = true;
            }

            if (valid) {
                var min_salary = $('#min_salary').val();
                var max_salary = $('#max_salary').val();
                var by_agreement = false;
                var city_id = $('#select-city').val();
                var subway_id = $('#select-subway').val();
                var company_id = $('#select-org').val();
                var schedule = $('select#select-schedule').val();
                var vacancy_description = $("#textEditorApp").Editor("getText");
                if ($('#by_agreement').is(':checked')){
                    by_agreement = true;
                }
                var data = {};
                data['post'] = $("#select-post").val();
                data['min_salary'] = min_salary;
                data['max_salary'] = max_salary;
                data['by_agreement'] = by_agreement;
                data['city_id'] = city_id;
                data['company_id'] = company_id;
                data['schedule'] = schedule;
                data['subway'] = subway_id;
                data['vacancy-description'] = vacancy_description;
                data['eyes'] = $('#select-eyes-color').val();
                data['hairs'] = $('#select-hairs').val();
                data['body_type'] = $('#select-body-type').val();
                data['weight'] = $('#select-weight').val();
                data['height'] = $('#select-height').val();
                data['zodiac'] = $('#select-zodiac').val();

                $(".level-selector-value").get().forEach(function (entry, index, array) {
                    data[$(entry).attr('id')] = $(entry).val();
                });

                if ($("#select-post").val()=='1') {
                    $(".level-slider-group-3").children().get().forEach(function (entry, index, array) {
                                    req_id = $(entry).children().find($('.level-selector-value'));
                                        $(req_id).each(function(idx, val) {
                                            level = $(val).find($('.level-selector-value'));
                                            var priority = $(level).attr('value');
                                            data[$(level).attr('id')] = priority;
                                        });
                                    });
                }
                else {
                    $(".third_div").children().get().forEach(function (entry, index, array) {
                    unit_id = $(entry).children().children();
                        $(unit_id).each(function(idx, val) {
                            level = $(val).find($('.level-selector-value'));
                            var priority = $(level).attr('value');
                            data['unit_block_' + $(val).attr('id')] = priority;

                        });
                    });
                }

                $(".level-slider-group-4").children().get().forEach(function (entry, index, array) {
                                    req_id = $(entry).children().find($('.level-selector-value'));
                                        $(req_id).each(function(idx, val) {
                                            level = $(val).find($('.level-selector-value'));
                                            var priority = $(level).attr('value');
                                            data[$(level).attr('id')] = priority;
                                        });
                                    });

                $(".level-slider-group-5").children().get().forEach(function (entry, index, array) {
                                    req_id = $(entry).children().find($('.level-selector-value'));
                                        $(req_id).each(function(idx, val) {
                                            level = $(val).find($('.level-selector-value'));
                                            var priority = $(level).attr('value');
                                            data[$(level).attr('id')] = priority;
                                        });
                                    });

                jsonArr = JSON.stringify(data);

                $.ajax({
                    type: 'POST',
                    url: '/restricted_employer/add_vacancy',
                    data: {"response": jsonArr,
                            "show_appearance" : true},
                    success: function () {
                        window.location.assign("/restricted_employer/");
                    },
                    error: function (xhr, status, err) {}
                });
            }

        }
        else {
            //form validation
            var valid = true;

            if ($('#select-city').val() == '') {
                valid = false;
                $('#city-error').show();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            else {
                $('#city-error').hide();
                valid = true;
            }

            if ($('#select-org').val() == null) {
                valid = false;
                $('#org-error').show();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            else {
                $('#org-error').hide();
                valid = true;
            }

            if ($('#select-schedule').val() == '') {
                valid = false;
                $('#schedule-error').show();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            else {
                $('#schedule-error').hide();
                valid = true;
            }

            if ($("#textEditorApp").Editor("getText") == '') {
                valid = false;
                $('#textarea-error').show();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            else {
                $('#textarea-error').hide();
                valid = true;
            }

            if (valid) {
                // get additional info
                var min_salary = $('#min_salary').val();
                var max_salary = $('#max_salary').val();
                var by_agreement = false;
                var city_id = $('#select-city').val();
                var subway_id = $('#select-subway').val();
                var company_id = $('#select-org').val();
                var schedule = $('select#select-schedule').val();
                var vacancy_description = $("#textEditorApp").Editor("getText");
                if ($('#by_agreement').is(':checked')){
                    by_agreement = true;
                }

                var data = {};
                data['post'] = $("#select-post").val();
                data['min_salary'] = min_salary;
                data['max_salary'] = max_salary;
                data['by_agreement'] = by_agreement;
                data['city_id'] = city_id;
                data['company_id'] = company_id;
                data['schedule'] = schedule;
                data['subway'] = subway_id;
                data['vacancy-description'] = vacancy_description;


                //get requirements from 2,3 and 4 steps
                $(".level-selector-value").get().forEach(function (entry, index, array) {
                    data[$(entry).attr('id')] = $(entry).val();
                });


                //get units priority
                $(".third_div").children().get().forEach(function (entry, index, array) {
                    unit_id = $(entry).children().children();
                    $(unit_id).each(function(idx, val) {
                        level = $(val).find($('.level-selector-value'));
                        var priority = $(level).attr('value');
                        data['unit_block_' + $(val).attr('id')] = priority;

                    });
                });


                //sending data
                jsonArr = JSON.stringify(data);
                $.ajax({
                    type: 'POST',
                    url: '/restricted_employer/add_vacancy',
                    data: {"response": jsonArr,
                            "show_appearance" : false,
                    },
                    success: function () {
                        window.location.assign("/restricted_employer/");
                    },
                    error: function (xhr, status, err) {
                                            }
                });
            }

        }

    }





