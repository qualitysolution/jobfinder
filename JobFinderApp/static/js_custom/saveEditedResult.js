
    function saveAll(withAppearance, salary) {
        if (withAppearance) {
            var data = {};
            data['show_appearance'] = true;
            data['post'] = $('#user_post').attr('post-id');
            data['salary'] = salary
            data['cv_about']=$("#textEditorApp").Editor("getText");
            data['eyes'] = $('#select-eyes-color').val();
            data['hairs'] = $('#select-hairs').val();
            data['body_type'] = $('#select-body-type').val();
            data['weight'] = $('#select-weight').val();
            data['height'] = $('#select-height').val();
            data['zodiac'] = $('#select-zodiac').val();

            $(".level-selector-value").get().forEach(function (entry, index, array) {
                data[$(entry).attr('id')] = $(entry).val();
            });

            if ($("#select-post").val()=='1') {
                $(".level-slider-group-3").children().get().forEach(function (entry, index, array) {
                                req_id = $(entry).children().find($('.level-selector-value'));
                                    $(req_id).each(function(idx, val) {
                                        level = $(val).find($('.level-selector-value'));
                                        var priority = $(level).attr('value');
                                        data[$(level).attr('id')] = priority;
                                    });
                                });
            }
            else {
                $(".third_div").children().get().forEach(function (entry, index, array) {
                unit_id = $(entry).children().children();
                    $(unit_id).each(function(idx, val) {
                        level = $(val).find($('.level-selector-value'));
                        var priority = $(level).attr('value');
                        data['unit_block_' + $(val).attr('id')] = priority;

                    });
                });
            }

            $(".level-slider-group-4").children().get().forEach(function (entry, index, array) {
                                req_id = $(entry).children().find($('.level-selector-value'));
                                    $(req_id).each(function(idx, val) {
                                        level = $(val).find($('.level-selector-value'));
                                        var priority = $(level).attr('value');
                                        data[$(level).attr('id')] = priority;
                                    });
                                });

            $(".level-slider-group-5").children().get().forEach(function (entry, index, array) {
                                req_id = $(entry).children().find($('.level-selector-value'));
                                    $(req_id).each(function(idx, val) {
                                        level = $(val).find($('.level-selector-value'));
                                        var priority = $(level).attr('value');
                                        data[$(level).attr('id')] = priority;
                                    });
                                });

            jsonArr = JSON.stringify(data);

            $.ajax({
                type: 'POST',
                url: '/restricted_applicant/edit_cv',
                data: {"response": jsonArr,
                        "record_id" : $('#user_post').attr('record-id'),
                        "show_appearance" : true,
                },
                success: function () {
                    window.location.assign("/restricted_applicant/");
                },
                error: function (xhr, status, err) {}
            });
        }
        else {
            // get additional info
            var data = {};
            data['show_appearance'] = false;
            data['post'] = $('#user_post').attr('post-id');
            data['salary'] = salary
            data['cv_about']=$("#textEditorApp").Editor("getText");


            //get requirements from 2,3 and 4 steps
            $(".level-selector-value").get().forEach(function (entry, index, array) {
                data[$(entry).attr('id')] = $(entry).val();
            });


            //get units priority
            $(".third_div").children().get().forEach(function (entry, index, array) {
                unit_id = $(entry).children().children();
                $(unit_id).each(function(idx, val) {
                    level = $(val).find($('.level-selector-value'));
                    var priority = $(level).attr('value');
                    data['unit_block_' + $(val).attr('id')] = priority;

                });
            });


            //sending data
            jsonArr = JSON.stringify(data);
            $.ajax({
                type: 'POST',
                url: '/restricted_applicant/edit_cv',
                data: {"response": jsonArr,
                        "record_id" : $('#user_post').attr('record-id'),
                        "show_appearance" : false,
                },
                success: function () {
                    window.location.assign("/restricted_applicant/");
                },
                error: function (xhr, status, err) {
                                        }
            });
        }

    }





