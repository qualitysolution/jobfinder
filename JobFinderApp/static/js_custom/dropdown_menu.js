/**
 * Created by Andrei on 20.07.16.
 */
$(document).ready(function () {
    $('li.form-dropdown a, li.dropdown a').on('click', function (event) {
        $(this).parent().toggleClass('open');
    });
    $('body').on('click', function (e) {
        if (!$('li.form-dropdown, li.dropdown').is(e.target)
            && $('li.form-dropdown, li.dropdown').has(e.target).length === 0
            && $('.open').has(e.target).length === 0
        ) {
            $('li.form-dropdown, li.dropdown').removeClass('open');
        }
    });
});