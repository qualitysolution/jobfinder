from django.shortcuts import render_to_response, HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.template.context_processors import csrf
from JobFinderApp.models import Employer

def employer_home(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/")
    return render_to_response("restricted_employer/home.html", RequestContext(request))


def about(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/")
    return render_to_response("restricted_employer/about.html", RequestContext(request))


def how_it_works(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/")
    return render_to_response("restricted_employer/how_it_works.html", RequestContext(request))


def cvs(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/")
    return render_to_response("restricted_employer/cvs.html", RequestContext(request))


def feedback(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/")
    return render_to_response("restricted_employer/feedback.html", RequestContext(request))


