from django.shortcuts import render_to_response, HttpResponseRedirect, HttpResponse, render
from django.template import RequestContext
from JobFinderApp.models import Employer, Post, EmployerVacancy, EmployerUser, ApplicationUser, Applicant, ApplicantCV
from JobFinderApp.models import EmployerVacancyRequirements, EmployerVacancyUnits, PostUnitGroup
from JobFinderApp.models import Unit, UnitGroup, Requirement, RequirementGroup
from JobFinderApp.models import Cities
from JobFinderApp.models import MetroStations, MetroLines
from django.template.context_processors import csrf
from django.core import serializers
from django.db import connection
import json
import re




def employer_vacancies(request):
    if not request.user.is_authenticated() or ApplicationUser.objects.filter(username=request.user).count() == 0:
        return HttpResponseRedirect("/")

    args = {}
    args.update(csrf(request))

    args["vacancies"] = EmployerVacancy.objects.filter(created_by=request.user)
    return render_to_response("restricted_employer/home.html", args, RequestContext(request))



def add_vacancy(request):
    if not request.user.is_authenticated() or ApplicationUser.objects.filter(username=request.user).count() == 0:
        return HttpResponseRedirect("/")

    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        args["posts"] = Post.objects.filter(is_active=True).order_by("name")
        args["employerUsers"] = EmployerUser.objects.filter(user=request.user)
        return render_to_response("restricted_employer/add_vacancy.html", args, RequestContext(request))
    elif request.method == "POST":
        post_data = json.loads(request.POST['response'])
        employer = Employer.objects.get(id=post_data["company_id"])
        vacancy = EmployerVacancy()
        vacancy.employer = employer
        vacancy.post = Post.objects.get(pk=post_data["post"])
        if (post_data["min_salary"]):
            vacancy.salary_lower = post_data["min_salary"]
        if (post_data["max_salary"]):
            vacancy.salary_upper = post_data["max_salary"]
        if (post_data["schedule"]):
            vacancy.schedule = post_data["schedule"]

        vacancy.salary_by_agreement = post_data["by_agreement"]
        if (post_data["city_id"]):
            vacancy.city = Cities.objects.get(id=post_data["city_id"])
        if (post_data["subway"]):
            vacancy.subway = MetroStations.objects.get(id=post_data["subway"])
        vacancy.created_by = ApplicationUser.objects.get(username=request.user)
        vacancy.description = post_data["vacancy-description"]
        if post_data["post"] == '1':
            vacancy.eyes_color = post_data['eyes']
            vacancy.weight = post_data['weight']
            vacancy.height = post_data['height']
            vacancy.body_type = post_data['body_type']
            vacancy.hairs = post_data['hairs']
            vacancy.zodiac = post_data['zodiac']
        vacancy.save()
        unit_block_re = re.compile('^unit_block_(\d+)$')
        requirement_re = re.compile('^requirement_(\d+)$')

        unit_ids = []
        requirement_ids = []
        for key, value in post_data.items():
            block_match = unit_block_re.search(key)
            if block_match != None:
                unit_ids.append(block_match.group(1))
            requirement_match = requirement_re.search(key)
            if requirement_match != None:
                requirement_ids.append(requirement_match.group(1))

        unit_list = list(Unit.objects.filter(id__in=unit_ids))
        requirement_list = list(Requirement.objects.filter(id__in=requirement_ids))

        new_units = []
        new_requirements = []

        for key, value in post_data.items():
            if unit_block_re.match(key):
                unit = EmployerVacancyUnits()
                unit.priority = int(value)
                m = unit_block_re.search(key)
                id = int(m.group(1))
                unit.unit = next(x for x in unit_list if x.id == id)
                unit.employer_vacancy = vacancy
                new_units.append(unit)
            elif requirement_re.match(key):
                m = requirement_re.search(key)
                id = int(m.group(1))
                requirement = EmployerVacancyRequirements()
                requirement.employer_vacancy = vacancy
                requirement.requirement = next(x for x in requirement_list if x.id == id)
                requirement.priority = int(value)
                new_requirements.append(requirement)

            else:
                continue
        EmployerVacancyUnits.objects.bulk_create(new_units)
        EmployerVacancyRequirements.objects.bulk_create(new_requirements)
        response = {'status': 1, 'message': "ok"}

        return HttpResponse(json.dumps(response), content_type='application/json')

def edit_vacancy(request):
    if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
        return HttpResponseRedirect("/")
    args = {}
    args.update(csrf(request))
    if request.method == "GET":
        data = EmployerVacancy.objects.get(id=request.GET["id"], created_by=request.user)
        args["post"] = data.post
        args["user"] = request.user.id
        args["record_id"] = request.GET["id"]
        args["employerUsers"] = EmployerUser.objects.filter(user=request.user)
        args["vacancy"] = EmployerVacancy.objects.get(id=request.GET["id"])
        args["cities"] = Cities.objects.all().order_by("name").values("id", "name")
        if MetroStations.objects.filter(line__city__id=data.city.id).exists():
            args["metro_stations"] = MetroStations.objects.filter(line__city__id=data.city.id).values("id", "name", "line__color", "line__name").order_by("name")
        return render_to_response("restricted_employer/edit_vacancy.html", args, RequestContext(request))
    if request.method == "POST":
        post_data = json.loads(request.POST['response'])
        employer = Employer.objects.get(id=post_data["company_id"])
        vacancy = EmployerVacancy.objects.get(id=request.POST['record_id'])
        vacancy.employer = employer
        vacancy.post = Post.objects.get(pk=post_data["post"])
        vacancy.created_by = ApplicationUser.objects.get(username=request.user)
        vacancy.description = post_data["vacancy-description"]
        if (post_data["min_salary"]):
            vacancy.salary_lower = post_data["min_salary"]
        if (post_data["max_salary"]):
            vacancy.salary_upper = post_data["max_salary"]
        if (post_data["schedule"]):
            vacancy.schedule = post_data["schedule"]

        vacancy.salary_by_agreement = post_data["by_agreement"]
        if (post_data["city_id"]):
            vacancy.city = Cities.objects.get(id=post_data["city_id"])
        if (post_data["subway"]):
            vacancy.subway = MetroStations.objects.get(id=post_data["subway"])

        if post_data["post"] == '1':
            vacancy.eyes_color = post_data['eyes']
            vacancy.weight = post_data['weight']
            vacancy.height = post_data['height']
            vacancy.body_type = post_data['body_type']
            vacancy.hairs = post_data['hairs']
            vacancy.zodiac = post_data['zodiac']
        vacancy.save()

        unit_block_re = re.compile('^unit_block_unit_id_(\d+)$')
        requirement_re = re.compile('^requirement_(\d+)$')
        unit_list = list(EmployerVacancyUnits.objects.filter(employer_vacancy=vacancy.id))
        requirement_list = list(EmployerVacancyRequirements.objects.filter(employer_vacancy=vacancy.id))
        changed_units= []
        changed_requirements = []

        for key, value in post_data.items():
            if unit_block_re.match(key):
                m = unit_block_re.search(key)
                id = int(m.group(1))
                unit = next(x for x in unit_list if x.unit_id == id)
                unit.priority = int(value)
                changed_units.append(unit)

            elif requirement_re.match(key):
                m = requirement_re.search(key)
                id = int(m.group(1))
                requirement = next(x for x in requirement_list if x.requirement_id == id)
                requirement.priority = int(value)
                changed_requirements.append(requirement)
            else:
                continue

        for i in range(1, 4):
            level_ids = [x.id for x in changed_requirements if x.priority == i]
            EmployerVacancyRequirements.objects.filter(id__in=level_ids).update(priority=i)

        for i in range(0, 6):
            level_ids = [x.id for x in changed_units if x.priority == i]
            if level_ids.count == 0:
                continue
            EmployerVacancyUnits.objects.filter(id__in=level_ids).update(priority=i)
    return HttpResponseRedirect("/restricted_employer/")


def remove_vacancy(request):
    if not request.user.is_authenticated() or ApplicationUser.objects.filter(username=request.user).count() == 0:
        return HttpResponseRedirect("/")
    args = {}
    args.update(csrf(request))
    if request.method == "GET":
        vacancy = EmployerVacancy.objects.get(id=request.GET["record_id"], created_by=request.user)
        vacancy.delete()
        args1 = {}
        args1.update(csrf(request))
        args1["vacancies"] = EmployerVacancy.objects.filter(created_by=request.user)
        return render_to_response("restricted_employer/home.html", args1, RequestContext(request))


def get_candidates(request):
    if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
        return HttpResponseRedirect("/")
    args = {}
    args.update(csrf(request))
    if request.method == 'GET':
        vacancy = EmployerVacancy.objects.get(id=request.GET['id'])
        args['post'] = vacancy.post_id
        cursor = connection.cursor()
        units = PostUnitGroup.objects.filter(post__id=vacancy.post_id)
        unit_grous = ["SUM(IF(units_ranks.group_id = {0}, units_ranks.points, NULL)) AS group_{0}".format(x.id) for x in units]
        group_fields = ", ".join(unit_grous)
        sql = '''
            SELECT
            cv.id,
            cv.additional_info,
            auth_user.first_name,
            auth_user.last_name,
            SUM(units_ranks.points) AS units_total,

            FROM
                JobFinderApp_applicantcv AS cv
                    LEFT JOIN
                (SELECT
                    JobFinderApp_applicantcv.id AS cv_id,
                        JobFinderApp_unitgroup.id AS group_id,
                        ((SUM(JobFinderApp_applicantcvunits.priority / POW(2, JobFinderApp_unitgroup.points_limit - JobFinderApp_employervacancyunits.priority)) - JobFinderApp_ratiounitrank.min) / (JobFinderApp_ratiounitrank.max - JobFinderApp_ratiounitrank.min)) AS points
                FROM
                    JobFinderApp_applicantcv
                LEFT JOIN JobFinderApp_applicantcvunits ON JobFinderApp_applicantcvunits.applicant_cv_id = JobFinderApp_applicantcv.id
                LEFT JOIN JobFinderApp_unit ON JobFinderApp_applicantcvunits.unit_id = JobFinderApp_unit.id
                LEFT JOIN JobFinderApp_unitgroup ON JobFinderApp_unitgroup.id = JobFinderApp_unit.group_id
                LEFT JOIN JobFinderApp_ratiounitrank ON JobFinderApp_unitgroup.points_limit = JobFinderApp_ratiounitrank.counter
                LEFT JOIN JobFinderApp_employervacancyunits ON JobFinderApp_employervacancyunits.unit_id = JobFinderApp_unit.id
                    AND JobFinderApp_employervacancyunits.employer_vacancy_id = {ev_id}
                WHERE
                    JobFinderApp_applicantcv.post_id = {post_id}
                GROUP BY JobFinderApp_applicantcv.id , JobFinderApp_unitgroup.id) AS units_ranks ON units_ranks.cv_id = cv.id
                    LEFT JOIN
                JobFinderApp_applicant ON cv.applicant_id = JobFinderApp_applicant.id
                    LEFT JOIN
                auth_user ON auth_user.id = JobFinderApp_applicant.user_id
            WHERE
                cv.post_id = {post_id}
            GROUP BY cv.id
            ORDER BY units_total DESC'''
        # .format(group_fields)
        cursor.execute(sql, {'fields' : group_fields,'post_id': vacancy.post_id, 'ed_id': vacancy.id})
        print(cursor.fetchall())
        # fields = ['id', 'date', 'hour', 'minute', 'interval']
        # dicts = [dict(zip(fields, d)) for d in data]
        # args['cvs'] = json.dumps(dicts)
        cursor.close()
        # for i in range(5):
        #     print(type(cursor.fetchone()))
    return render_to_response("restricted_employer/candidates.html", args, RequestContext(request))