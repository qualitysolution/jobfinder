from django.shortcuts import render_to_response, HttpResponseRedirect, HttpResponse, render
from django.template import RequestContext
from JobFinderApp.models import Applicant, Post, ApplicantCV, ApplicantCVRequirements, ApplicantCVUnits
from JobFinderApp.models import Unit, UnitGroup, Requirement, RequirementGroup
from django.template.context_processors import csrf
from django.core import serializers
import json
import re

def applicant_cv(request):
    if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
        return HttpResponseRedirect("/")

    args = {}
    args.update(csrf(request))
    args["cvs"] = ApplicantCV.objects.filter(applicant__user=request.user)
    return render_to_response("restricted_applicant/home.html", args, RequestContext(request))


def add_cv(request):
    if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
        return HttpResponseRedirect("/")

    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        args["posts"] = Post.objects.filter(is_active=True).order_by("name").values()
        return render_to_response("restricted_applicant/add_cv.html", args, RequestContext(request))

    elif request.method == "POST":
        post_data = json.loads(request.POST['response'])
        applicant = Applicant.objects.get(user=request.user)
        cv = ApplicantCV()
        cv.applicant = applicant
        cv.post = Post.objects.get(pk=post_data["post"])
        cv.salary = post_data["salary"]
        cv.additional_info = post_data["cv_about"] #request.POST["applicantAbout"]

        cv.save()

        unit_block_re = re.compile('^unit_block_(\d+)$')
        requirement_re = re.compile('^requirement_(\d+)$')

        unit_ids = []
        requirement_ids = []
        for key, value in post_data.items():
            block_match = unit_block_re.search(key)
            if block_match != None:
                unit_ids.append(block_match.group(1))
            requirement_match = requirement_re.search(key)
            if requirement_match != None:
                requirement_ids.append(requirement_match.group(1))

        unit_list = list(Unit.objects.filter(id__in=unit_ids))
        requirement_list = list(Requirement.objects.filter(id__in=requirement_ids))

        new_units = []
        new_requirements = []

        for key, value in post_data.items():
            if unit_block_re.match(key):
                unit = ApplicantCVUnits()
                unit.priority = int(value)
                m = unit_block_re.search(key)
                id = int(m.group(1))
                unit.unit = next(x for x in unit_list if x.id == id)
                unit.applicant_cv = cv
                new_units.append(unit)
            elif requirement_re.match(key):
                m = requirement_re.search(key)
                id = int(m.group(1))
                requirement = ApplicantCVRequirements()
                requirement.applicant_cv = cv
                requirement.requirement = next(x for x in requirement_list if x.id == id)
                requirement.priority = int(value)
                new_requirements.append(requirement)

            else:
                continue
        ApplicantCVUnits.objects.bulk_create(new_units)
        ApplicantCVRequirements.objects.bulk_create(new_requirements)


        response = {'status': 1, 'message': "ok"}
        return HttpResponse(json.dumps(response), content_type='application/json')

def remove_cv(request):
    if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
        return HttpResponseRedirect("/")
    args = {}
    args.update(csrf(request))
    if request.method == "GET":
        cv = ApplicantCV.objects.get(id=request.GET["record_id"], applicant__user=request.user)
        cv.delete()
        response = {'status': 0}
        #return HttpResponse(json.dumps(response), content_type='application/json')
        #return HttpResponseRedirect("/restricted_applicant/")
        args1 = {}
        args1.update(csrf(request))
        args1["cvs"] = ApplicantCV.objects.filter(applicant__user=request.user)
        return render_to_response("restricted_applicant/home.html", args1, RequestContext(request))

def edit_cv(request):
    if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
        return HttpResponseRedirect("/")
    args = {}
    args.update(csrf(request))
    if request.method == "GET":
        data = ApplicantCV.objects.get(id=request.GET["id"], applicant__user=request.user)
        args["post"] = data.post
        args["user"] = request.user.id
        args["record_id"] = request.GET["id"]
        return render_to_response("restricted_applicant/edit_cv.html", args, RequestContext(request))

    if request.method == "POST":
        post_data = json.loads(request.POST['response'])
        cv = ApplicantCV.objects.get(id=request.POST['record_id'])
        cv.salary = post_data["salary"]
        cv.additional_info = post_data["cv_about"]
        cv.save()
        # if post_data["show_appearance"]:
        #     cv.eyes_color = post_data['eyes']
        #     cv.weight = post_data['weight']
        #     cv.height = post_data['height']
        #     cv.body_type = post_data['body_type']
        #     cv.hairs = post_data['hairs']
        #     cv.zodiac = post_data['zodiac']


        unit_block_re = re.compile('^unit_block_unit_id_(\d+)$')
        requirement_re = re.compile('^requirement_(\d+)$')

        unit_list = list(ApplicantCVUnits.objects.filter(applicant_cv=cv.id))
        requirement_list = list(ApplicantCVRequirements.objects.filter(applicant_cv=cv.id))
        changed_units = []
        changed_requirements = []

        for key, value in post_data.items():
            if unit_block_re.match(key):
                m = unit_block_re.search(key)
                id = int(m.group(1))
                unit = next(x for x in unit_list if x.unit_id == id)
                unit.priority = int(value)
                changed_units.append(unit)

            elif requirement_re.match(key):
                m = requirement_re.search(key)
                id = int(m.group(1))
                requirement = next(x for x in requirement_list if x.requirement_id == id)
                requirement.priority = int(value)
                changed_requirements.append(requirement)
            else:
                continue

        for i in range(1, 4):
            level_ids = [x.id for x in changed_requirements if x.priority == i]
            ApplicantCVRequirements.objects.filter(id__in=level_ids).update(priority=i)

        for i in range(0, 6):
            level_unit_ids = [x.id for x in changed_units if x.priority == i]
            if level_unit_ids.count == 0:
                continue
            ApplicantCVUnits.objects.filter(id__in=level_unit_ids).update(priority=i)

    return HttpResponseRedirect("/restricted_applicant/")

