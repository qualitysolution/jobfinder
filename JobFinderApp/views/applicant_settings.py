from django.shortcuts import render_to_response, HttpResponse
from django.template import RequestContext
from JobFinderApp.models import Applicant, EmployerContactEmail
from JobFinderApp.models import ApplicantEducation
from JobFinderApp.models import ApplicantContactPhone
from JobFinderApp.models import ApplicantContactEmail
from JobFinderApp.models import ApplicantAppearances
from JobFinderApp.models import Cities
from JobFinderApp.models import MetroStations
from django.http import HttpResponseRedirect
from django.template.context_processors import csrf
from datetime import datetime
from django.core import serializers
from django.core.mail import send_mail
import json
import uuid
from JobFinder.settings import DEFAULT_FROM_EMAIL
from JobFinder.settings import SERVER_IPI


def settings(request):
    args = {}
    args.update(csrf(request))
    applicant = Applicant()
   
    if request.method == "GET":
        if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
            return HttpResponseRedirect("/")
        applicant = Applicant.objects.get(user=request.user)

    elif request.method == "POST":
        applicant = Applicant.objects.get(user=request.user)
        applicant.user.first_name = request.POST["firstName"]

        applicant.user.last_name = request.POST["lastName"]
        applicant.user.patronymic = request.POST["patronymic"]
        applicant.nationality = 1
        nationality = request.POST.get('nationality', 0)

        if 'nationality' not in request.POST:
            applicant.nationality = 0
        else:
            applicant.nationality = 1
    

              
        applicant.about = request.POST["applicantAbout"]
        applicant.date_of_birth = datetime.strptime(request.POST["dateOfBirth"], "%d.%m.%Y").date()
        applicant.gender = int(request.POST["gender"])

        if "city" in request.POST:
            applicant.city = Cities.objects.get(id=request.POST["city"])
        else:
            applicant.city = None
        if "subway" in request.POST:
            applicant.subway = MetroStations.objects.get(id=request.POST["subway"])
        else:
            applicant.subway = None
        applicant.save()
        
        if request.POST["password"]:
            applicant.user.set_password(request.POST["password"])
            applicant.user.save()
            args["success_message"] = "Пароль быль изменен. Теперь требуется заново авторизоваться."
            return render_to_response('public/show_message.html', args, RequestContext(request))
        else:
            applicant.user.save()
            

    args["applicant"] = Applicant.objects.get(user=request.user)
    args["cities"] = Cities.objects.all().order_by("name")
    if applicant.city:
        metro_stations = MetroStations.objects.filter(line__city_id=applicant.city.id).order_by('name')
        if metro_stations.count() > 0:
            args["metro_stations"] = metro_stations
    
    return render_to_response('restricted_applicant/settings.html', args, RequestContext(request))



def add_appearance(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        if ApplicantAppearances.objects.filter(applicant__user=request.user).exists():
            applicant = Applicant.objects.get(user=request.user)
            appearance = ApplicantAppearances.objects.get(applicant__user=request.user)
            appearance.applicant = applicant
            appearance.eyes_color = request.POST["eyes_color"]
            appearance.hair = request.POST["hair"]
            appearance.body_type = request.POST["body_type"]
            appearance.weight = request.POST["weight"]
            appearance.height = request.POST["height"]
            appearance.zodiac = request.POST["zodiac"]
            response = {'status': 0}
            appearance.save()
        else:
            applicant = Applicant.objects.get(user=request.user)
            appearance = ApplicantAppearances()
            appearance.applicant = applicant
            appearance.eyes_color = request.POST["eyes_color"]
            appearance.hair = request.POST["hair"]
            appearance.body_type = request.POST["body_type"]
            appearance.weight = request.POST["weight"]
            appearance.height = request.POST["height"]
            appearance.zodiac = request.POST["zodiac"]
            response = {'status': 0}
            appearance.save()
        return HttpResponse(json.dumps(response), content_type='application/json')


def add_education(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        applicant = Applicant.objects.get(user=request.user)
        education = ApplicantEducation()
        education.applicant = applicant
        education.institution_name = request.POST["institution_name"]
        education.education_type = int(request.POST["education_type"])
        education.finish_date = request.POST["finish_date"]
        education.speciality = request.POST["speciality"]
        education.save()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def remove_education(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        education = ApplicantEducation.objects.get(id=request.GET["record_id"])
        education.delete()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def edit_education(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        education = ApplicantEducation.objects.get(id=request.POST["record_id"])
        education.institution_name = request.POST["institution_name"]
        education.education_type = int(request.POST["education_type"])
        education.finish_date = request.POST["finish_date"]
        education.speciality = request.POST["speciality"]
        education.save()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def get_education(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        data = serializers.serialize('json', ApplicantEducation.objects.filter(id=request.GET["record_id"]))
        response = HttpResponse()
        response['Content-Type'] = "text/javascript"
        response.write(data)
        return response


def add_phone(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        applicant = Applicant.objects.get(user=request.user)
        phone = ApplicantContactPhone()
        phone.applicant = applicant
        phone.phone = request.POST["phone"]
        # phone.use_for_notification = True if request.POST["use_for_notification"] == "true" else False
        phone.show_to_employer = True if request.POST["show_to_employer"] == "true" else False
        phone.save()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def remove_phone(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        phone = ApplicantContactPhone.objects.get(id=request.GET["record_id"])
        phone.delete()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def edit_phone(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        phone = ApplicantContactPhone.objects.get(id=request.POST["record_id"])
        phone.phone = request.POST["phone"]
        # phone.use_for_notification = True if request.POST["use_for_notification"] == "true" else False
        phone.show_to_employer = True if request.POST["show_to_employer"] == "true" else False
        phone.save()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def get_phone(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        data = serializers.serialize('json', ApplicantContactPhone.objects.filter(id=request.GET["record_id"]))
        response = HttpResponse()
        response['Content-Type'] = "text/javascript"
        response.write(data)
        return response


def add_email(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        applicant = Applicant.objects.get(user=request.user)
        email = ApplicantContactEmail()
        count = ApplicantContactEmail.objects.filter(email=request.POST["email"], applicant=applicant).count()
        if count < 1:
            email.applicant = applicant
            email.email = request.POST["email"]
            email.use_for_notification = True if request.POST["use_for_notification"] == "true" else False
            email.show_to_employer = True if request.POST["show_to_employer"] == "true" else False
            if request.user.username == email.email:
                email.confirmed = True
            else:
                email.confirmed = False
                email.activation_code = str(uuid.uuid4())
                message = "Привет, " + request.user.first_name + '!\n' \
                                                                 "Для активации нового e-mail пройди по данной ссылке: " \
                                                                 "http://" + SERVER_IPI + "/activate_applicant_email?activation_code=" + email.activation_code
                send_mail('Активация', message, DEFAULT_FROM_EMAIL, [email.email], fail_silently=False)
            email.save()
            response = {'status': 0}
            return HttpResponse(json.dumps(response), content_type='application/json')
        else:
            response = {'status': 1, 'message': "Такой E-mail уже существует!"}
            return HttpResponse(json.dumps(response), content_type='application/json')


def remove_email(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        email = ApplicantContactEmail.objects.get(id=request.GET["record_id"])
        email.delete()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def edit_email(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        email = ApplicantContactEmail.objects.get(id=request.POST["record_id"])
        email.email = request.POST["email"]
        email.use_for_notification = True if request.POST["use_for_notification"] == "true" else False
        email.show_to_employer = True if request.POST["show_to_employer"] == "true" else False
        email.save()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def get_email(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        data = serializers.serialize('json', ApplicantContactEmail.objects.filter(id=request.GET["record_id"]))
        response = HttpResponse()
        response['Content-Type'] = "text/javascript"
        response.write(data)
        return response


def activate_applicant_email(request):
    args = {}
    args.update(csrf(request))
    if request.method == 'GET':
        activation_code = request.GET['activation_code']
        emails = ApplicantContactEmail.objects.filter(activation_code=activation_code)
        if emails.count() < 1:
            args['error_message'] = "Активация учетной записи по данной ссылке более не доступна. " \
                                    "Возможно это связано с тем, что она уже была произведена."
            return render_to_response('public/show_message.html', args, RequestContext(request))
        email = emails[0]
        email.confirmed = True
        email.activation_code = None
        email.save()
        args['success_message'] = "Активация e-mail прошла успешно. "
        return render_to_response('public/show_message.html', args, RequestContext(request))


def reactivate_email(request):
    args = {}
    args.update(csrf(request))
    if request.method == "POST":
        email = ApplicantContactEmail.objects.get(id=request.POST["record_id"])
        name = email.applicant.last_name + ' ' + email.applicant.first_name
        if email.activation_code == None:
            email.activation_code = str(uuid.uuid4())
            email.save()
        message = "Привет, " + name + '!\n' \
                                      "Для активации нового e-mail пройди по данной ссылке: " \
                                      "http://" + SERVER_IPI + "/activate_applicant_email?activation_code=" + email.activation_code
        send_mail('Активация', message, DEFAULT_FROM_EMAIL, [email.email], fail_silently=False)
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')
    else:
        response = {'status': 1, 'message': "Такой E-mail уже существует!"}
        return HttpResponse(json.dumps(response), content_type='application/json')


def set_avatar(request):
    args = {}
    args.update(csrf(request))
    if request.method == "POST":
        applicant = Applicant.objects.get(user=request.user)
        photo = request.FILES["files"]
        x1 = request.POST["x1"]
        x2 = request.POST["x2"]
        y1 = request.POST["y1"]
        y2 = request.POST["y2"]
        extension = photo.name.split(".")[-1].lower()
        if not (extension == "png" or extension == "jpeg" or extension == "jpg"):
            response = {'status': 1}
        else:
            applicant.photo = photo
            applicant.x1 = x1
            applicant.x2 = x2
            applicant.y1 = y1
            applicant.y2 = y2
            applicant.save()
            response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')
    else:
        response = {'status': 1}
        return HttpResponse(json.dumps(response), content_type='application/json')
