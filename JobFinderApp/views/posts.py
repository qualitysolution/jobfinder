from JobFinderApp.models import Requirement, PostRequirementGroup, Post, Unit, PostUnitGroup, UnitGroup, RequirementGroup
from JobFinderApp.models import Applicant, Post, ApplicantCV, ApplicantCVRequirements, ApplicantCVUnits
from JobFinderApp.models import EmployerVacancyRequirements, EmployerVacancyUnits, EmployerVacancy
from django.shortcuts import HttpResponse
from django.template.context_processors import csrf
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers
from rest_framework.serializers import ModelSerializer
from rest_framework.renderers import JSONRenderer
import json


class UnitSerializer(ModelSerializer):
    class Meta:
        model = Unit
        fields = ('id', 'name')


class UnitGroupSerializer(ModelSerializer):
    units = UnitSerializer(many=True)

    class Meta:
        model = UnitGroup
        fields = ('id', 'name', 'points_limit', 'units')


class PostSerializer(ModelSerializer):
    unit_groups = UnitGroupSerializer(many=True)

    class Meta:
        model = Post
        fields = ('id', 'name', 'unit_groups')



def get_requirements(request):
    if request.method == "GET":
        post_requirements = PostRequirementGroup.objects.get(post__id=request.GET["post"], step=request.GET["step"])
        requirements = Requirement.objects.filter(group__id=post_requirements.group.id).order_by("name").values("id", "name")
        data = RequirementGroup.objects.get(id=post_requirements.group.id)
        response = {}
        response["id"] = data.id
        response["name"] = data.name
        response["high_rating_count"] = data.high_rating_count
        response["medium_rating_count"] = data.medium_rating_count
        response["requirements"] = list(requirements)
        response["status"] = 0
        if 'record_id' in request.GET:
            if 'employer' in request.GET:
                current_priority = EmployerVacancyRequirements.objects.filter(employer_vacancy__id=request.GET["record_id"]).filter(
                requirement__group__id=post_requirements.group.id).values()
            else:
                current_priority = ApplicantCVRequirements.objects.filter(applicant_cv__id=request.GET["record_id"]).filter(
                requirement__group__id=post_requirements.group.id).values()
            response["current_priority"] = list(current_priority)


        return HttpResponse(json.dumps(response), content_type='application/json')


def get_post(request):
    if request.method == "GET":
        data = Post.objects.get(id=request.GET["post"])
        response = {}
        response["show_appearance"] = data.show_appearance
        return HttpResponse(json.dumps(response), content_type='application/json')


def has_unit_groups(request):
    if request.method == "GET":
        result = PostUnitGroup.objects.filter(post__id=request.GET["post"])
        if result.count() > 0:
            response = {'status': 0, 'message': "true"}
        else:
            response = {'status': 0, 'message': "false"}
        return HttpResponse(json.dumps(response), content_type='application/json')


def get_unit_groups(request):
    if request.method == "GET":
        result = Post.objects.get(id=request.GET["post"])
        response = HttpResponse()
        response['Content-Type'] = "text/javascript"
        temp = JSONRenderer().render(PostSerializer(result).data).decode(encoding='UTF-8')
        response.write("[" + temp + "]")
        return response


def current_unit_priority(request):
    if request.method == "GET":
        result = Post.objects.get(id=request.GET["post"])
        if 'employer' in request.GET:
            priority = EmployerVacancyUnits.objects.filter(employer_vacancy__id=request.GET["record_id"]).filter(employer_vacancy__post=result).values()
        else:
            priority = ApplicantCVUnits.objects.filter(applicant_cv__id=request.GET['record_id']).filter(applicant_cv__post=result).values()
        return HttpResponse(json.dumps(list(priority)), content_type='application/json')


def get_additional_info(request):
    if request.method == "GET":
        response = {}

        if 'employer' in request.GET:
            additional = EmployerVacancy.objects.get(id=request.GET['record_id'])
            response["min_salary"] = additional.salary_lower
            response["max_salary"] = additional.salary_upper
            response["by_agreement"] = additional.salary_by_agreement
            response["schedule"] = additional.schedule
            response["city"] = additional.city.id
            if additional.subway != None:
                response["subway"] = additional.subway.id

            response["eyes_color"] = additional.eyes_color
            response["hairs"] = additional.hairs
            response["body_type"] = additional.body_type
            response["weight"] = additional.weight
            response["height"] = additional.height
            response["zodiac"] = additional.zodiac
            response["description"] = additional.description
        else:
            additional = ApplicantCV.objects.get(id=request.GET['record_id'])
            response["cv_about"] = additional.additional_info
            response["cv_salary"] = additional.salary


        return HttpResponse(json.dumps(response), content_type='application/json')
