from django.shortcuts import render_to_response
from django.template import RequestContext
from JobFinderApp.models import EmployerUser
from JobFinderApp.models import ApplicationUser
from django.http import HttpResponseRedirect
from django.template.context_processors import csrf


def settings(request):
    args = {}
    args.update(csrf(request))
    user = ApplicationUser()

    if request.method == "GET":
        if not request.user.is_authenticated() or ApplicationUser.objects.filter(username=request.user).count() == 0:
            return HttpResponseRedirect("/")
        user = EmployerUser.objects.filter(user=request.user)[0].user
    elif request.method == "POST":
        user = EmployerUser.objects.filter(user=request.user)[0].user
        user.last_name = request.POST.get("lastName", user.last_name)
        user.first_name = request.POST.get("firstName", user.first_name)
        user.patronymic = request.POST.get("patronymic", user.patronymic)
        user.save()
        if request.POST["password"]:
            request.user.set_password(request.POST["password"])
            request.user.save()
            args["success_message"] = "Пароль быль изменен. Теперь требуется заново авторизоваться."
            return render_to_response('public/show_message.html', args, context_instance=RequestContext(request))
    args["user"] = user
    return render_to_response("restricted_employer/user_settings.html", args, RequestContext(request))
