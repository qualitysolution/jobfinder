from django.shortcuts import render_to_response, HttpResponse
from django.template import RequestContext
from JobFinderApp.models import EmployerUser
from JobFinderApp.models import ApplicationUser
from JobFinderApp.models import Employer
from JobFinderApp.models import EmployerContactEmail
from JobFinderApp.models import EmployerContactPhone
from JobFinderApp.models import Cities
from JobFinderApp.models import MetroStations
from django.http import HttpResponseRedirect
from django.template.context_processors import csrf
from django.core import serializers
from django.core.mail import send_mail
import json
import uuid
from JobFinder.settings import DEFAULT_FROM_EMAIL
from JobFinder.settings import SERVER_IPI


def settings(request):
    args = {}
    args.update(csrf(request))
    employer = Employer()

    if request.method == "GET":
        if not request.user.is_authenticated():
            return HttpResponseRedirect("/")
        employerQuerySet = EmployerUser.objects.filter(user=request.user)
        if "organizationId" in request.GET:
            employer = Employer.objects.get(id=request.GET["organizationId"])
        else:
            if employerQuerySet.count() > 0:
                employer = employerQuerySet[0].employer

    elif request.method == "POST":
        employer = Employer.objects.get(id=request.POST["organizationId"])
        employer.org_name = request.POST["organizationName"]
        employer.site_address = request.POST["organizationSite"]
        employer.address = request.POST["organizationAddress"]
        employer.about = request.POST["organizationAbout"]
        employer.employees_number = int(request.POST["employeesCount"])

        if "city" in request.POST:
            employer.city = Cities.objects.get(id=request.POST["city"])
        else:
            employer.city = None
        if "subway" in request.POST:
            employer.subway = MetroStations.objects.get(id=request.POST["subway"])
        else:
            employer.subway = None
        employer.save()
    args["employerUsers"] = EmployerUser.objects.filter(user=request.user)
    args["employer"] = employer
    args["organization"] = employer
    args["cities"] = Cities.objects.all().order_by("name")
    if employer.city:
        metro_stations = MetroStations.objects.filter(line__city_id=employer.city.id).order_by('name')
        if metro_stations.count() > 0:
            args["metro_stations"] = metro_stations



    return render_to_response("restricted_employer/employer_settings.html", args, RequestContext(request))


def add_phone(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        employer = EmployerUser.objects.get(user=request.user).employer
        phone = EmployerContactPhone()
        phone.employer = employer
        phone.phone = request.POST["phone"]
        # phone.use_for_notification = True if request.POST["use_for_notification"] == "true" else False
        phone.show_to_applicant = True if request.POST["show_to_applicant"] == "true" else False
        phone.save()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def remove_phone(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        phone = EmployerContactPhone.objects.get(id=request.GET["record_id"])
        phone.delete()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def edit_phone(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        phone = EmployerContactPhone.objects.get(id=request.POST["record_id"])
        phone.phone = request.POST["phone"]
        # phone.use_for_notification = True if request.POST["use_for_notification"] == "true" else False
        phone.show_to_applicant = True if request.POST["show_to_applicant"] == "true" else False
        phone.save()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def get_phone(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        data = serializers.serialize('json', EmployerContactPhone.objects.filter(id=request.GET["record_id"]))
        response = HttpResponse()
        response['Content-Type'] = "text/javascript"
        response.write(data)
        return response


def add_email(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        employer = EmployerUser.objects.get(user=request.user).employer
        email = EmployerContactEmail()
        count = EmployerContactEmail.objects.filter(email=request.POST["email"], employer=employer).count()
        if count == 0:
            email.employer = employer
            email.email = request.POST["email"]
            email.use_for_notification = True if request.POST["use_for_notification"] == "true" else False
            email.show_to_applicant = True if request.POST["show_to_applicant"] == "true" else False
            if request.user.username == email.email:
                email.confirmed = True
            else:
                email.confirmed = False
                email.activation_code = str(uuid.uuid4())
                message = "Привет, " + employer.org_name + '!\n' \
                                                           "Для активации нового e-mail пройди по данной ссылке: " \
                                                           "http://" + SERVER_IPI + "/activate_employer_email?activation_code=" + email.activation_code
                send_mail('Активация', message, DEFAULT_FROM_EMAIL, [email.email], fail_silently=False)
                email.save()
                response = {'status': 0}
            return HttpResponse(json.dumps(response), content_type='application/json')
        else:
            response = {'status': 1, 'message': "Такой E-mail уже существует!"}
            return HttpResponse(json.dumps(response), content_type='application/json')


def invite(request):
    args = {}
    args.update(csrf(request))
    if request.method == "POST":
        employer = EmployerUser.objects.get(user=request.user).employer
        email = request.POST["email"]
        users = ApplicationUser.objects.filter(username=email)
        if users.count() > 0:
            employerUser = EmployerUser()
            employerUser.user = users[0]
            employerUser.employer = employer
            employerUser.save()
            response = {'status': 0}
        else:
            message = "Привет! Организация " + employer.org_name + \
                      " желает добавить вас в список своих сотрудников. Для подтверждения зарегистрируйтесь на данном сайте " \
                      "http://" + SERVER_IPI
            send_mail('Активация', message, DEFAULT_FROM_EMAIL, [email], fail_silently=False)
            response = {'status': 1, 'message': "Сообщение с приглашением зарегистрироваться было отправлено на почту."}
        return HttpResponse(json.dumps(response), content_type='application/json')


def remove_email(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        email = EmployerContactEmail.objects.get(id=request.GET["record_id"])
        email.delete()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def edit_email(request):
    args = {}
    args.update(csrf(request))

    if request.method == "POST":
        email = EmployerContactEmail.objects.get(id=request.POST["record_id"])
        email.email = request.POST["email"]
        email.use_for_notification = True if request.POST["use_for_notification"] == "true" else False
        email.show_to_applicant = True if request.POST["show_to_applicant"] == "true" else False
        email.save()
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')


def get_email(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        data = serializers.serialize('json', EmployerContactEmail.objects.filter(id=request.GET["record_id"]))
        response = HttpResponse()
        response['Content-Type'] = "text/javascript"
        response.write(data)
        return response


def activate_employer_email(request):
    args = {}
    args.update(csrf(request))
    if request.method == 'GET':
        activation_code = request.GET['activation_code']
        emails = EmployerContactEmail.objects.filter(activation_code=activation_code)
        if emails.count() < 1:
            args['error_message'] = "Активация учетной записи по данной ссылке более не доступна. " \
                                    "Возможно это связано с тем, что она уже была произведена."
            return render_to_response('public/show_message.html', args, context_instance=RequestContext(request))
        email = emails[0]
        email.confirmed = True
        email.activation_code = None
        email.save()
        args['success_message'] = "Активация e-mail прошла успешно. "
        return render_to_response('public/show_message.html', args, context_instance=RequestContext(request))


def reactivate_email(request):
    args = {}
    args.update(csrf(request))
    if request.method == "POST":
        email = EmployerContactEmail.objects.get(id=request.POST["record_id"])
        employer = EmployerUser.objects.get(user=request.user).employer
        if email.activation_code == None:
            email.activation_code = str(uuid.uuid4())
            email.save()
        message = "Привет, " + employer.org_name + '!\n' \
                                                   "Для активации нового e-mail пройди по данной ссылке: " \
                                                   "http://" + SERVER_IPI + "/activate_employer_email?activation_code=" + email.activation_code
        send_mail('Активация', message, DEFAULT_FROM_EMAIL, [email.email], fail_silently=False)
        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')
    else:
        response = {'status': 1, 'message': "Такой E-mail уже существует!"}
        return HttpResponse(json.dumps(response), content_type='application/json')


def register_employer(request):
    args = {}
    args.update(csrf(request))
    if request.method == "POST":
        employer = Employer()
        employer.org_name = request.POST["organization_name"]
        employer.site_address = request.POST["organization_site"]
        employer.employees_number = int(request.POST["employees_count"])
        employer.save()

        employer_user = EmployerUser()
        employer_user.employer = employer
        employer_user.user = ApplicationUser.objects.get(pk=request.user.id)
        employer_user.save()

        response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')
    else:
        response = {'status': 1, 'message': "Такой E-mail уже существует!"}
        return HttpResponse(json.dumps(response), content_type='application/json')


def set_logo(request):
    args = {}
    args.update(csrf(request))
    if request.method == "POST":
        employer = Employer.objects.get(id=request.POST["organizationId"])
        logo = request.FILES["files"]
        x1 = request.POST["x1"]
        x2 = request.POST["x2"]
        y1 = request.POST["y1"]
        y2 = request.POST["y2"]
        extension = logo.name.split(".")[-1].lower()
        if not (extension == "png" or extension == "jpeg" or extension == "jpg"):
            response = {'status': 1}
        else:
            employer.logo = logo
            employer.x1 = x1
            employer.x2 = x2
            employer.y1 = y1
            employer.y2 = y2
            employer.save()
            response = {'status': 0}
        return HttpResponse(json.dumps(response), content_type='application/json')
    else:
        response = {'status': 1}
        return HttpResponse(json.dumps(response), content_type='application/json')
