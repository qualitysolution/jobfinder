from django.shortcuts import render_to_response, HttpResponseRedirect
from django.template import RequestContext
from JobFinderApp.models import Applicant


def about(request):
    if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
        return HttpResponseRedirect("/")
    return render_to_response("restricted_applicant/about.html", RequestContext(request))


def how_it_works(request):
    if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
        return HttpResponseRedirect("/")
    return render_to_response("restricted_applicant/how_it_works.html", RequestContext(request))


def vacancies(request):
    if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
        return HttpResponseRedirect("/")
    return render_to_response("restricted_applicant/vacancies.html", RequestContext(request))


def feedback(request):
    if not request.user.is_authenticated() or Applicant.objects.filter(user=request.user).count() == 0:
        return HttpResponseRedirect("/")
    return render_to_response("restricted_applicant/feedback.html", RequestContext(request))
