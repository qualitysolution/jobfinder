from django.shortcuts import render_to_response
from django.template import RequestContext
from JobFinderApp.models import ApplicationUser, Applicant, Employer, EmployerUser, EmailConfirmation, PasswordRestore, User
from django.core.mail import send_mail
from django.template.context_processors import csrf
from datetime import datetime
import uuid
from JobFinder.settings import DEFAULT_FROM_EMAIL
from JobFinder.settings import SERVER_IPI


def applicant_registration(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        return render_to_response("public/applicant_registration.html", args, RequestContext(request))
    if request.method == "POST":
        user_exists = ApplicationUser.objects.filter(username=request.POST["email"]).count()
        if user_exists > 0:
            args["error_message"] = "Пользователь с таким email уже зарегистрирован."
            args = {**args, **request.POST.dict()}
            return render_to_response("public/applicant_registration.html", args, RequestContext(request))
        user = ApplicationUser()
        user.username = request.POST["email"]
        user.email = user.username
        user.is_superuser = False
        user.is_staff = False
        user.is_active = False
        user.first_name = request.POST["firstName"]
        user.last_name = request.POST["lastName"]
        user.set_password(request.POST["password"])
        user.first_name = request.POST["firstName"]
        user.last_name = request.POST["lastName"]
        user.patronymic = request.POST["patronymic"]
        user.save()
        applicant = Applicant()
        applicant.date_of_birth = datetime.strptime(request.POST["dateOfBirth"], "%d.%m.%Y").date()
        applicant.gender = int(request.POST["gender"])
        applicant.user = user
        applicant.save()

        activation_key = str(uuid.uuid4())
        confirmation = EmailConfirmation()
        confirmation.activation_code = activation_key
        confirmation.user = user
        confirmation.save()
        # TODO: real link and email
        message = "Привет, " + applicant.user.first_name + '!\n' \
                                                           "Для активации учетной записи пройди по данной ссылке: " \
                                                           "http://" + SERVER_IPI + "/activate_account?activation_code=" + activation_key

        send_mail('Активация', message, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)

        args['email'] = applicant.user.email
        return render_to_response('public/activation_needed.html', args, RequestContext(request))


def employer_registration(request):
    args = {}
    args.update(csrf(request))
    if request.method == "GET":
        return render_to_response("public/employer_registration.html", args, RequestContext(request))
    if request.method == "POST":
        user_exists = ApplicationUser.objects.filter(username=request.POST["email"]).count()
        if user_exists > 0:
            args["error_message"] = "Пользователь с таким email уже зарегистрирован. Восстановлением пароля, если забыли пароль"
            args = {**args, **request.POST.dict()}
            return render_to_response("public/employer_registration.html", args, RequestContext(request))

        user = ApplicationUser()
        user.username = request.POST["email"]
        user.email = user.username
        user.is_superuser = False
        user.is_staff = False
        user.is_active = False
        user.first_name = request.POST["firstName"]
        user.last_name = request.POST["lastName"]
        user.patronymic = request.POST["patronymic"]
        user.set_password(request.POST["password"])
        user.save()

        applicant = Applicant()
        applicant.date_of_birth = datetime.strptime(request.POST["dateOfBirth"], "%d.%m.%Y").date()
        applicant.gender = int(request.POST["gender"])
        applicant.user = user
        applicant.save()

        employer = Employer()
        employer.org_name = request.POST["organizationName"]
        employer.site_address = request.POST["organizationSite"]
        employer.employees_number = int(request.POST["employeesCount"])
        employer.save()

        employer_user = EmployerUser()
        employer_user.employer = employer
        employer_user.user = user
        employer_user.save()

        activation_key = str(uuid.uuid4())
        confirmation = EmailConfirmation()
        confirmation.activation_code = activation_key
        confirmation.user = user
        confirmation.save()
        # TODO: real link and email
        message = "Привет, " + user.first_name + '!\n' \
                                                 "Для активации учетной записи пройди по данной ссылке: " \
                                                 "http://" + SERVER_IPI + "/activate_account?activation_code=" + activation_key

        send_mail('Активация', message, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
        args['email'] = user.email
        args['success_message'] = "Для завершения процедуры регистрации, пожалуйста, перейдите по ссылке в письме, " \
                                  "которое было отправлено на указанный вами адрес электронной почты."

        return render_to_response('public/show_message.html', args, RequestContext(request))


def activate_account(request):
    args = {}
    args.update(csrf(request))
    if request.method == 'GET':
        activation_code = request.GET['activation_code']
        confirmation = EmailConfirmation.objects.filter(activation_code=activation_code)
        if confirmation.count() < 1:
            args['error_message'] = "Активация учетной записи по данной ссылке более не доступна. " \
                                    "Возможно это связано с тем, что она уже была произведена."
            return render_to_response('public/show_message.html', args, RequestContext(request))

        user = confirmation[0].user
        user.is_active = True
        user.save()
        confirmation.delete()
        args['success_message'] = "Активация прошла успешно. " \
                                  "Теперь вы можете войти, используя данные вашей учетной записи."
        return render_to_response('public/show_message.html', args, RequestContext(request))


def request_restore(request):
    args = {}
    args.update(csrf(request))
    if request.method == 'GET':
        return render_to_response('public/request_restore_password.html', args, RequestContext(request))
    if request.method == "POST":
        email = request.POST["email"]
        users = User.objects.filter(username=email)
        if users.count() < 1:
            args["error_message"] = "Пользователь с таким email не зарегистрирован."
            return render_to_response('public/request_restore_password.html', args, RequestContext(request))
        else:
            user = users[0]
            restore_objects = PasswordRestore.objects.filter(user=user)
            if restore_objects.count() < 1:
                restore_key = str(uuid.uuid4())
                restore = PasswordRestore()
                restore.restore_code = restore_key
                restore.user = user
                restore.save()
            else:
                restore_key = restore_objects[0].restore_code
            message = "Для восстановления пароля вашей учетной записи пройдите по данной ссылке: " \
                      "http://" + SERVER_IPI + "/restore_password?restore_code=" + restore_key + \
                      " Если вы не запрашивали восстановление пароля - просто проигнорируйте данное письмо."

            send_mail('Восстановление пароля', message, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
            args["success_message"] = "Ссылка для восстановления пароля была отправлена вам на почту."
            return render_to_response('public/request_restore_password.html', args, RequestContext(request))


def restore_password(request):
    args = {}
    args.update(csrf(request))
    if request.method == 'GET':
        restore_code = request.GET['restore_code']
        restore_objects = PasswordRestore.objects.filter(restore_code=restore_code)
        if restore_objects.count() < 1:
            args['error_message'] = "Ссылка на восстановление пароля более не действительна."
            return render_to_response('public/show_message.html', args, RequestContext(request))

        args['restore_code'] = restore_code
        return render_to_response('public/restore_password.html', args, RequestContext(request))
    if request.method == 'POST':
        restore_code = request.POST['restore_code']
        restore_objects = PasswordRestore.objects.filter(restore_code=restore_code)
        if restore_objects.count() < 1:
            args['error_message'] = "Ссылка на восстановление пароля более не действительна."
            return render_to_response('public/show_message.html', args, RequestContext(request))

        restore = restore_objects[0]
        user = restore.user
        user.set_password(request.POST["password"])
        user.save()
        restore.delete()
        args['success_message'] = "Новый пароль был успешно установлен. Теперь вы можете авторизоваться."
        return render_to_response('public/show_message.html', args, RequestContext(request))
