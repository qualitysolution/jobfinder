from django.shortcuts import render_to_response, HttpResponseRedirect
from django.template import RequestContext
from JobFinderApp.models import EmployerUser, Applicant
from django.template.context_processors import csrf
from datetime import datetime, timedelta


def home(request):
    args = {}
    args.update(csrf(request))

    if request.user.is_authenticated():
        return redirect_authenticated(request)
    return render_to_response("public/home.html", args, RequestContext(request))


def about(request):
    args = {}
    args.update(csrf(request))
    if request.user.is_authenticated():
        return redirect_authenticated(request)
    return render_to_response("public/about.html", args, RequestContext(request))


def how_it_works(request):
    args = {}
    args.update(csrf(request))
    if request.user.is_authenticated():
        return redirect_authenticated(request)
    return render_to_response("public/how_it_works.html", args, RequestContext(request))


def vacancies(request):
    args = {}
    args.update(csrf(request))
    if request.user.is_authenticated():
        return redirect_authenticated(request)
    return render_to_response("public/vacancies.html", args, RequestContext(request))


def cvs(request):
    args = {}
    args.update(csrf(request))
    if request.user.is_authenticated():
        return redirect_authenticated(request)
    return render_to_response("public/cvs.html", args, RequestContext(request))


def feedback(request):
    args = {}
    args.update(csrf(request))
    if request.user.is_authenticated():
        return redirect_authenticated(request)
    return render_to_response("public/feedback.html", args, RequestContext(request))


def redirect_authenticated(request):
    args = {}
    args.update(csrf(request))

    if 'last_login' in request.COOKIES:
        val = request.COOKIES['last_login']
        if val == 'applicant':
            return HttpResponseRedirect("/restricted_applicant")
        else:
            return HttpResponseRedirect("/restricted_employer")
    else:
        max_age = 365 * 24 * 60 * 60
        expires = datetime.strftime(datetime.utcnow() + timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
        if Applicant.objects.filter(user=request.user).count() > 0:
            response = HttpResponseRedirect("/restricted_applicant")
            response.set_cookie("last_login", "applicant", max_age=max_age, expires=expires)
            return response
        else:
            response = HttpResponseRedirect("/restricted_employer")
            response.set_cookie("last_login", "employer", max_age=max_age, expires=expires)
            return response
