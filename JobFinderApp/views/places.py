from django.shortcuts import HttpResponse
from JobFinderApp.models import Cities, MetroStations
from JobFinderApp.models import Applicant
from django.template.context_processors import csrf
from django.core.serializers.json import DjangoJSONEncoder
import json


def get_cities(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        
        cities = Cities.objects.all().order_by("name").values("id", "name")
        data =  json.dumps(list(cities), cls=DjangoJSONEncoder)
        return HttpResponse(data, content_type="application/json")


def get_subway(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        stations = MetroStations.objects.filter(line__city__id=request.GET["city"]).values("id", "name", "line__color", "line__name").order_by("name")
        data = json.dumps(list(stations), cls=DjangoJSONEncoder)
        return HttpResponse(data, content_type='application/json')
