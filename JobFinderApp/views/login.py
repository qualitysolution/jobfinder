from django.shortcuts import render_to_response, HttpResponseRedirect
from django.template import RequestContext
from JobFinderApp.models import Applicant, EmployerUser
from django.contrib import auth
from django.template.context_processors import csrf
from datetime import datetime, timedelta

def login(request):
    args = {}
    args.update(csrf(request))

    if request.method == "GET":
        return render_to_response("public/login.html", RequestContext(request))
    if request.method == "POST":
        type = request.POST["type"]
        email = request.POST["email"]
        password = request.POST["password"]
        max_age = 365 * 24 * 60 * 60
        expires = datetime.strftime(datetime.utcnow() + timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")

        user = auth.authenticate(username=email, password=password)
        if user is None:
            args["error_message"] = "Ошибка авторизации. Проверьте введенные данные."
            return render_to_response('public/login.html', args, RequestContext(request))

        if not user.is_active:
            args["error_message"] = "Учетная запись не была активирована."
            return render_to_response('public/login.html', args, RequestContext(request))

        if type == "applicant":
            applicant = Applicant.objects.filter(user=user)
            if not applicant.exists():
                args["error_message"] = "Ошибка авторизации. Проверьте введенные данные."
                return render_to_response('public/login.html', args, RequestContext(request))
            auth.login(request, user)
            response = HttpResponseRedirect("/restricted_applicant/")
            response.set_cookie("last_login", "applicant", max_age=max_age, expires=expires)
            return response
        else:
            auth.login(request, user)
            response = HttpResponseRedirect("restricted_employer/") #TODO fixme
            response.set_cookie("last_login", "employer", max_age=max_age, expires=expires)
            return response


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("/")
