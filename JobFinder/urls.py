from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

from JobFinderApp.views.home import home
from JobFinderApp.views.home import about as public_about
from JobFinderApp.views.home import how_it_works as public_how_it_works
from JobFinderApp.views.home import vacancies as public_vacancies
from JobFinderApp.views.home import cvs as public_cvs
from JobFinderApp.views.home import feedback as public_feedback

from JobFinderApp.views.registration import applicant_registration
from JobFinderApp.views.registration import employer_registration
from JobFinderApp.views.registration import activate_account
from JobFinderApp.views.registration import request_restore
from JobFinderApp.views.registration import restore_password


from JobFinderApp.views.places import get_cities
from JobFinderApp.views.places import get_subway

from JobFinderApp.views.posts import get_requirements
from JobFinderApp.views.posts import get_post
from JobFinderApp.views.posts import has_unit_groups
from JobFinderApp.views.posts import get_unit_groups
from JobFinderApp.views.posts import get_unit_groups, current_unit_priority
from JobFinderApp.views.posts import get_additional_info



from JobFinderApp.views.applicant_settings import settings as applicant_settings
from JobFinderApp.views.applicant_settings import set_avatar
from JobFinderApp.views.applicant_settings import activate_applicant_email
from JobFinderApp.views.applicant_settings import reactivate_email as applicant_reactivate_email

from JobFinderApp.views.applicant_settings import add_education
from JobFinderApp.views.applicant_settings import get_education
from JobFinderApp.views.applicant_settings import remove_education
from JobFinderApp.views.applicant_settings import edit_education

from JobFinderApp.views.applicant_settings import add_appearance

from JobFinderApp.views.applicant_settings import add_phone as applicant_add_phone
from JobFinderApp.views.applicant_settings import get_phone as applicant_get_phone
from JobFinderApp.views.applicant_settings import remove_phone as applicant_remove_phone
from JobFinderApp.views.applicant_settings import edit_phone as applicant_edit_phone

from JobFinderApp.views.applicant_settings import add_email as applicant_add_email
from JobFinderApp.views.applicant_settings import get_email as applicant_get_email
from JobFinderApp.views.applicant_settings import remove_email as applicant_remove_email
from JobFinderApp.views.applicant_settings import edit_email as applicant_edit_email

from JobFinderApp.views.user_settings import settings as user_settings

from JobFinderApp.views.employer_settings import settings as employer_settings
from JobFinderApp.views.employer_settings import activate_employer_email
from JobFinderApp.views.employer_settings import reactivate_email as employer_reactivate_email
from JobFinderApp.views.employer_settings import set_logo
from JobFinderApp.views.employer_settings import invite

from JobFinderApp.views.employer_settings import add_phone as employer_add_phone
from JobFinderApp.views.employer_settings import get_phone as employer_get_phone
from JobFinderApp.views.employer_settings import remove_phone as employer_remove_phone
from JobFinderApp.views.employer_settings import edit_phone as employer_edit_phone

from JobFinderApp.views.employer_settings import register_employer
from JobFinderApp.views.employer_settings import add_email as employer_add_email
from JobFinderApp.views.employer_settings import get_email as employer_get_email
from JobFinderApp.views.employer_settings import remove_email as employer_remove_email
from JobFinderApp.views.employer_settings import edit_email as employer_edit_email

from JobFinderApp.views.applicant_home import vacancies as applicant_vacancies
from JobFinderApp.views.applicant_home import about as applicant_about
from JobFinderApp.views.applicant_home import how_it_works as applicant_how_it_works
from JobFinderApp.views.applicant_home import feedback as applicant_feedback

from JobFinderApp.views.applicant_cv import applicant_cv
from JobFinderApp.views.applicant_cv import add_cv
from JobFinderApp.views.applicant_cv import remove_cv
from JobFinderApp.views.applicant_cv import edit_cv

from JobFinderApp.views.employer_home import cvs as employer_cvs
from JobFinderApp.views.employer_home import about as employer_about
from JobFinderApp.views.employer_home import how_it_works as employer_how_it_works
from JobFinderApp.views.employer_home import feedback as employer_feedback
from JobFinderApp.views.employer_home import employer_home
from JobFinderApp.views.employer_vacancies import add_vacancy,remove_vacancy, edit_vacancy, employer_vacancies
from JobFinderApp.views.employer_vacancies import get_candidates

from JobFinderApp.views.login import login
from JobFinderApp.views.login import logout

urlpatterns = [
    url(r'^$', home, name='home'),

    url(r'^applicant_registration$', applicant_registration, name='applicant_registration'),
    url(r'^employer_registration$', employer_registration, name='employer_registration'),

    url(r'^restricted_applicant/?$', applicant_cv, name='applicant_home'),
    url(r'^restricted_applicant/add_cv$', add_cv, name='applicant_home'),
    url(r'^restricted_applicant/remove_cv$', remove_cv, name='applicant_home'),
    url(r'^restricted_applicant/edit_cv$', edit_cv, name='edit_cv'),


    url(r'^restricted_applicant/settings/?$', applicant_settings, name='applicant_settings'),
    url(r'^restricted_applicant/add_appearance/?$', add_appearance, name='add_appearance'),
    url(r'^restricted_applicant/vacancies/?$', applicant_vacancies, name='applicant_vacancies'),
    url(r'^restricted_applicant/about/?$', applicant_about, name='applicant_about'),
    url(r'^restricted_applicant/how_it_works/?$', applicant_how_it_works, name='applicant_how_it_works'),
    url(r'^restricted_applicant/feedback/?$', applicant_feedback, name='applicant_feedback'),

    url(r'^restricted_applicant/set_avatar', set_avatar, name='set_avatar'),
    url(r'^restricted_applicant/add_education$', add_education, name='add_education'),
    url(r'^restricted_applicant/get_education$', get_education, name='get_education'),
    url(r'^restricted_applicant/remove_education$', remove_education, name='remove_education'),
    url(r'^restricted_applicant/edit_education$', edit_education, name='edit_education'),

    url(r'^restricted_applicant/add_phone$', applicant_add_phone, name='applicant_add_phone'),
    url(r'^restricted_applicant/get_phone$', applicant_get_phone, name='applicant_get_phone'),
    url(r'^restricted_applicant/remove_phone$', applicant_remove_phone, name='applicant_remove_phone'),
    url(r'^restricted_applicant/edit_phone$', applicant_edit_phone, name='applicant_edit_phone'),

    url(r'^restricted_applicant/reactivate_email$', applicant_reactivate_email, name='reactivate_email'),
    url(r'^restricted_applicant/add_email$', applicant_add_email, name='applicant_add_email'),
    url(r'^restricted_applicant/get_email$', applicant_get_email, name='applicant_get_email'),
    url(r'^restricted_applicant/remove_email$', applicant_remove_email, name='applicant_remove_email'),
    url(r'^restricted_applicant/edit_email$', applicant_edit_email, name='applicant_edit_email'),

    url(r'^restricted_employer/reactivate_email$', employer_reactivate_email, name='reactivate_email'),
    url(r'^restricted_employer/register_employer$', register_employer, name='register_employer'),
    url(r'^restricted_employer/add_phone$', employer_add_phone, name='employer_add_phone'),
    url(r'^restricted_employer/invite$', invite, name='invite'),
    url(r'^restricted_employer/get_phone$', employer_get_phone, name='employer_get_phone'),
    url(r'^restricted_employer/remove_phone$', employer_remove_phone, name='employer_remove_phone'),
    url(r'^restricted_employer/edit_phone$', employer_edit_phone, name='employer_edit_phone'),

    url(r'^restricted_employer/settings/?$', employer_settings, name='employer_settings'),
    url(r'^restricted_employer/add_email$', employer_add_email, name='employer_add_email'),
    url(r'^restricted_employer/get_email$', employer_get_email, name='employer_get_email'),
    url(r'^restricted_employer/set_logo', set_logo, name='set_logo'),
    url(r'^restricted_employer/remove_email$', employer_remove_email, name='employer_remove_email'),
    url(r'^restricted_employer/edit_email$', employer_edit_email, name='employer_edit_email'),

    url(r'^restricted_employer/?$', employer_vacancies, name='employer_vacancies'),
    #add vacancies
    url(r'^restricted_employer/add_vacancy$', add_vacancy, name='employer_home'),
    url(r'^restricted_employer/edit_vacancy$', edit_vacancy, name='employer_home'),
    url(r'^restricted_employer/remove_vacancy$', remove_vacancy, name='applicant_home'),
    url(r'^restricted_employer/get_candidates$', get_candidates, name='applicant_home'),
    url(r'^restricted_employer/user_settings', user_settings, name='user_settings'),
    url(r'^restricted_employer/employer_settings', employer_settings, name='employer_settings'),
    url(r'^restricted_employer/cvs/?$', employer_cvs, name='employer_cvs'),
    url(r'^restricted_employer/about/?$', employer_about, name='employer_about'),
    url(r'^restricted_employer/how_it_works/?$', employer_how_it_works, name='employer_how_it_works'),
    url(r'^restricted_employer/feedback/?$', employer_feedback, name='employer_feedback'),

    url(r'^public/get_requirements', get_requirements, name='get_requirements'),
    url(r'^public/get_post', get_post, name='get_post'),
    url(r'^public/has_unit_groups', has_unit_groups, name='has_unit_groups'),
    url(r'^public/get_unit_groups', get_unit_groups, name='get_unit_groups'),
    url(r'^public/current_unit_priority', current_unit_priority, name='current_unit_priority'),
    url(r'^public/get_additional_info', get_additional_info, name='get_additional_info'),


    url(r'^login$', login, name='login'),
    url(r'^logout$', logout, name='logout'),
    url(r'^activate_account', activate_account, name='activate_account'),
    url(r'^request_restore_password', request_restore, name='request_restore'),
    url(r'^restore_password', restore_password, name='restore_password'),
    url(r'^activate_applicant_email', activate_applicant_email, name='activate_applicant_email'),
    url(r'^activate_employer_email', activate_employer_email, name='activate_employer_email'),

    url(r'^public/about', public_about, name='public_about'),
    url(r'^public/vacancies', public_vacancies, name='public_vacancies'),
    url(r'^public/how_it_works', public_how_it_works, name='public_how_it_works'),
    url(r'^public/cvs', public_cvs, name='public_cvs'),
    url(r'^public/feedback', public_feedback, name='public_feedback'),

    
    url(r'^public/get_cities', get_cities, name='get_cities'),
    url(r'^public/get_subway', get_subway, name='get_subway'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
