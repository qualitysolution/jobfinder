#!/bin/bash
#cd /opt/Innova/jobfinder
echo 'Killing server...'
pkill python3
echo 'Updating repository...'
git stash
git pull
git stash apply
echo 'Starting server...'
nohup python3 ./manage.py runserver 0.0.0.0:8000 &
echo 'Completed'
#cd -
